//Author: Michael Folkerts, mmfolkerts@gmail.com, http://bit.ly/folkerts
//Date: March 2012

#include "drrSuite.cuh" //Mike's new library
#include <iostream>
using std::cout;
using std::endl;

#include <fstream>
using std::ofstream;
using std::ifstream;

#include <algorithm>
using std::fill;

//using namespace std;

/** a simple file saving template */
template <class T>
void saveDataFile(const char* fileName, T* array, long int numElements){
	// save the data
	ofstream outFile(fileName,ifstream::binary);
	outFile.write((char*)array,sizeof(T)*numElements);
	outFile.close();
	cout << "Wrote " << sizeof(T)*numElements << " bytes to file: " << fileName << endl;
}

int main (void){

	cout << endl;

	// create new drrSuite object
	drrSuite* traceTest = new drrSuite(0,TRILINEAR); //GPU ID, algorithm
	bool errorCheck;
	// simple GPU API test
	cout << "Running drrSuite on GPU #" << traceTest->getGPU() << endl;

	// voxel volume information
	int    volDim[]  = {256,256,128};
	float  voxSize[] = {2.0f,2.0f,2.5f}; //mm
	float* volArray  = new float[ volDim[0]*volDim[1]*volDim[2] ];
	// fill the volume with uniform density
	fill(volArray, volArray+volDim[0]*volDim[1]*volDim[2], 1.0f);

	// set voxel information on GPU
	traceTest->setVoxels(volDim, voxSize, volArray);

	/*
	 * This example starts out with the more general form of the getProjection
	 * which will take an arbitrary detector (endPts) geometry that the user
	 * must generate. This function has a consiterably higher overhead cost
	 * since the endPts arrays must be copied into memory for each projection.
	 * Note that the traceArray must be created by the user and have the
	 * correct length.
	 */

	cout << "\n** Testing general DRR mode **" << endl;

	// projection information
	int   detectorWidth  = 512;
	int   detectorHeight = 512;
	float isocenter[]    = {float(volDim[0])*voxSize[0]/2.0,float(volDim[1])*voxSize[1]/2.0,float(volDim[2])*voxSize[2]/2.0}; // mm (vector from corner of data to origin)
	// place the upper-left corner of the detector:
	float detectorUL[]   = {-500.0,-(float(detectorWidth)/2.0),float(detectorHeight)/2.0}; // mm
	// detector unit vectors
	float pixDn[] = {0.0,0.0,-1.0*(512.0f/float(detectorHeight))}; // mm
	float pixRt[] = {0.0,1.0*(512.0f/float(detectorWidth)),0}; // mm

	//drrSuite parameters
	float  startPt[]    = {1000.0,0.0,0.0};             // starting point of ray trace
	int    endPtsLength = detectorWidth*detectorHeight; // length of the X Y and Z end point arrays
	float* endPtsX = new float[ endPtsLength ];         // defines the X element of the ending points
	float* endPtsY = new float[ endPtsLength ];         // defines the Y element of the ending points
	float* endPtsZ = new float[ endPtsLength ];         // defines the Z element of the ending points
	float* volOffset  = isocenter;                      // the origin of coord. sys. relative to (0,0,0) corner of volume data (isocenter)
	float* traceArray = new float[endPtsLength];        // RESULT: array for the result of the ray traces

	//set trace array to zero:
	fill(traceArray, traceArray+endPtsLength, 0.0);

	//generate simple detector geometry
	for(int row=0; row<detectorHeight; row++){
		for(int col=0; col<detectorWidth; col++){
			endPtsX[detectorWidth*row+col] = detectorUL[0] + pixDn[0]*row + pixRt[0]*col;
			endPtsY[detectorWidth*row+col] = detectorUL[1] + pixDn[1]*row + pixRt[1]*col;
			endPtsZ[detectorWidth*row+col] = detectorUL[2] + pixDn[2]*row + pixRt[2]*col;
		}
	}

	// test dry run to see overhead costs
	traceTest->dryRun = true; 
	errorCheck = traceTest->getProjection(startPt, endPtsX, endPtsY, endPtsZ, endPtsLength, volOffset, traceArray);

	// test one projection
	traceTest->dryRun = false; 
	errorCheck = traceTest->getProjection(startPt, endPtsX, endPtsY, endPtsZ, endPtsLength, volOffset, traceArray);

	// save the data
	saveDataFile("single_projection.floats",traceArray, endPtsLength);
	cout << "Projection is " << detectorWidth << " cols by " << detectorHeight << " cols, stored as "
	     << sizeof(float) <<"-byte floats in row-major order." << endl;

	/*
	 * Next we show how to use the simpler CBCT interface which is faster
	 * since the detector geometry is generated on the GPU and the only
	 * memory transfer is at the end when the projection data is copied back
	 * to the host (unless one chooses to pass device pointers or run in economy mode).
	 */

	//test CBCT mode
	cout << "\n** Testing CBCT DRR mode **" << endl;

	// use the DRR Suite (ds) CBCT class
	ds_CBCT* testCBCT = new ds_CBCT();
		// DEFAULTS: SID = 1500 mm, SAD = 1000 mm
		// 640x480 pixel detector of size 400x300 mm
		// isocenter = (0,0,0)
		// 36 projections, one every 10 degrees starting from 0 deg

	// the following can be set by hand:
	testCBCT->setDetector(640,480,400.0f,300.0f);                   // columns, rows, width (mm), height (mm)
	testCBCT->setIsocenter(isocenter[0],isocenter[1],isocenter[2]); // mm
	testCBCT->setAnglesDeg(100,0.0f,1.8f);                          // number of angles, start angle, angle step size
	testCBCT->setShifts(0.0f,0.0f);                                 // detector shifts (right,up) from beams-eye-view (mm)

	// reset traceArray to be large enough to fit all the projections
	delete traceArray;
	traceArray = new float[testCBCT->getN()]; 
	fill(traceArray, traceArray+testCBCT->getN(), 0.0);

	// generate projections in cbct mode by passing the ds_CBCT object to the drrSuite member function
	traceTest->dryRun = true;
	errorCheck = traceTest->getProjection(testCBCT, traceArray);

	traceTest->dryRun = false;
	errorCheck = traceTest->getProjection(testCBCT, traceArray);

	// save the data
	saveDataFile("cbct_projections.floats", traceArray, testCBCT->getN());
	cout << "Projection is " << testCBCT->getCols() << " cols by " << testCBCT->getRows()
	     << " rows by " << testCBCT->getNumAngles() << " layers, stored as "
	     << sizeof(float) << "-byte floats in row-major order." << endl;


	/*
	 * Last we consider an "economy" (GPU RAM saving) mode that copies back each projection
	 * after it is rendered. The approach requires only enough RAM on the GPU to
	 * store the voxel data and a single projection.
	 */

	//test Economy mode
	cout << "\n** Testing CBCT DRR in Economy mode **" << endl;

	//clean up traceArray
	fill(traceArray, traceArray+testCBCT->getN(), 0.0);

	//set ecomony mode
	traceTest->economy = true;

	traceTest->dryRun = true;
	errorCheck = traceTest->getProjection(testCBCT, traceArray);

	traceTest->dryRun = false;
	traceTest->getProjection(testCBCT, traceArray);

	// save the data
	saveDataFile("cbct_projections_econo.floats", traceArray, testCBCT->getN());
	cout << "Projection is " << testCBCT->getCols() << " cols by " << testCBCT->getRows()
	     << " rows by " << testCBCT->getNumAngles() << " layers, stored as "
	     << sizeof(float) << "-byte floats in row-major order." << endl;

	//cleanup
	cout << endl;
	delete testCBCT;
	//traceTest->resetGPU();
	delete traceTest;
	delete volArray;
	delete endPtsX;
	delete endPtsY;
 	delete endPtsZ;
	delete traceArray;
}
