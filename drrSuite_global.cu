/** Global namespace for CUDA constants textures and kernels 
 */
#ifndef DRR_SUITE_GLOBAL_NS
#define DRR_SUITE_GLOBAL_NS

namespace DRR_SUITE_GLOBAL{

	float PI = acos(-1.0f);

	//simple global timer API (adapted from quickCudaLib)
	///TODO: maybe move this timer into the class istelf?
	float elapsed_time = 0.0f;
	int   timer_dev;
	int   eventflags;
	//int   context; //used to preserve context over timer call
	cudaEvent_t start_event, stop_event;
	inline void initTimer(){
		//DS_ERROR_CHECK( cudaGetDevice(&timer_dev) );
		eventflags = cudaEventBlockingSync;
		DS_ERROR_CHECK( cudaEventCreateWithFlags(&start_event, eventflags) );
		DS_ERROR_CHECK( cudaEventCreateWithFlags(&stop_event,  eventflags) );
	}
	inline void startTimer(){
		//DS_ERROR_CHECK(cudaGetDevice(&context));
		//DS_ERROR_CHECK(cudaSetDevice(timer_dev));
		DS_ERROR_CHECK(cudaEventRecord(start_event, 0));
		//DS_ERROR_CHECK(cudaSetDevice(context));
	}
	inline void stopTimer(){
		//DS_ERROR_CHECK(cudaGetDevice(&context));
		//DS_ERROR_CHECK(cudaSetDevice(timer_dev));
		DS_ERROR_CHECK(cudaDeviceSynchronize());
		DS_ERROR_CHECK(cudaEventRecord(stop_event, 0));
		DS_ERROR_CHECK(cudaEventSynchronize(stop_event));
		DS_ERROR_CHECK(cudaEventElapsedTime(&elapsed_time, start_event, stop_event));
		//DS_ERROR_CHECK(cudaSetDevice(context));
	}
	inline float readTimer(){
		return elapsed_time;
	}

	inline void cleanupTimer(){
		//DS_ERROR_CHECK(cudaGetDevice(&context));
		//DS_ERROR_CHECK(cudaSetDevice(timer_dev));
		DS_ERROR_CHECK(cudaEventDestroy(start_event));
		DS_ERROR_CHECK(cudaEventDestroy(stop_event));
		//DS_ERROR_CHECK(cudaSetDevice(context));
	}

	/// todo: sort constants by algorithm:

	//3Dtexture:
	texture<float, 3, cudaReadModeElementType> tex_vox;

	//2D texture for FIXEDGRID
	texture<float, 2, cudaReadModeElementType> tex_rot_vox2D;
	texture<float, 3, cudaReadModeElementType> tex_rot_vox3D;

    //backwards compatability at compile time:
    #if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ < 200)
    #warning skipping surface declarations for CUDA Arch < 2.0
    #else
    #warning surfaces defined
    surface<void,  2> transVox_surf2D; //allows writing to a texture
	surface<void,  3> transVox_surf3D; //allows writing to a texture
    #endif

	//device constants:
	__constant__ float dc_voxPlanesMin[3]; //plane locations
	__constant__ float dc_voxPlanesMax[3]; //plane locations
	__constant__ float dc_source[3];       //source point
	__constant__ int   dc_voxDim[3];       //size of each dimension
	__constant__ float dc_voxDim_f[3];       //size of each dimension
	__constant__ float dc_voxSize[3];      //spacing of CT planes
	__constant__ float dc_corner[3];       //spacing of CT planes
	__constant__ float dc_pixelDown[3];    //spacing of CT planes
	__constant__ float dc_pixelRight[3];   //spacing of CT planes
	__constant__ int   dc_maxElement;      //maxElement in projection
	__constant__ int   dc_blocksPerRow;    //maxElement in projection
	__constant__ bool  dc_CBCTmode;        //specifies CBCT mode for initSiddon
	__constant__ float dc_samplingFreq;    //fixed stride sampling freq. (1/mm)

	__constant__ float* dc_rotatedVoxData_ptr;   //using in backwards compatable fixed grid

	// helper functions	
	__device__ inline float* max_4(float* v4){
		float* max = v4;

		#pragma unroll
		for( int i=1; i<4; i++){
			if( *max < *(v4+i) )
				max = v4+i;
		}
		return max;
	}

	__device__ inline float* min_4(float* v4){
		float* min = v4;
		#pragma unroll
		for( int i=1; i<4; i++){
			if( *min > *(v4+i) )
				min = v4+i;
		}
		return min;
	}

__device__ void siddonInitTraceParameters_EXP(const float *ray, float *alphaMin, float *alphaMax, int *startDim)
//      each ray perform forward split process
{
                float temp;
                //int startDim;
                *alphaMin = 0.0F;
                *alphaMax = 1.0F;
                float alpha1, alphan;

                alpha1 = (-dc_voxDim[0]/2*dc_voxSize[0] - dc_source[0])/ray[0];//(xi - xs);
                alphan = ( dc_voxDim[0]/2*dc_voxSize[0] - dc_source[0])/ray[0];//(xi - xs);
                temp = min(alpha1, alphan);
                *alphaMin = max(*alphaMin, temp);
                if(*alphaMin == temp) *startDim = 0;
                temp = max(alpha1, alphan);
                *alphaMax = min(*alphaMax, temp);

                alpha1 = (-dc_voxDim[1]/2*dc_voxSize[1] - dc_source[1])/ray[1];//(yi - ys);
                alphan = ( dc_voxDim[1]/2*dc_voxSize[1] - dc_source[1])/ray[1];//(yi - ys);
                temp = min(alpha1, alphan);
                *alphaMin = max(*alphaMin, temp);
                if(*alphaMin == temp) *startDim = 1;
                temp = max(alpha1, alphan);
                *alphaMax = min(*alphaMax, temp);

                alpha1 = (-dc_voxDim[2]/2*dc_voxSize[2] - dc_source[2])/ray[2];//(zi - zs);
                alphan = ( dc_voxDim[2]/2*dc_voxSize[2] - dc_source[2])/ray[2];//(zi - zs);
                temp = min(alpha1, alphan);
                *alphaMin = max(*alphaMin, temp);
                if(*alphaMin == temp) *startDim = 2;
                temp = max(alpha1, alphan);
                *alphaMax = min(*alphaMax, temp);
}

	/** Initializes the parametric ray trace parameters for Siddon's algorithm) */
	__device__ inline void siddonInitTraceParameters(const float* ray,float* ti, float* tf, int* startDim){

		float iMin[3],iMax[3];
		//compute parametric voxel plane intersections
		#pragma unroll
		for(int i=0;i<3;i++){
			iMin[i] = ( dc_voxPlanesMin[i] - dc_source[i] ) / ray[i];//tmin for each dimension
			iMax[i] = ( dc_voxPlanesMax[i] - dc_source[i] ) / ray[i];//tmax for each dimension
		}
		//SIDDON'S WAY:
		float  tmax[] = {max(iMin[0],iMax[0]),max(iMin[1],iMax[1]),max(iMin[2],iMax[2]),1.0f};
		float  tmin[] = {min(iMin[0],iMax[0]),min(iMin[1],iMax[1]),min(iMin[2],iMax[2]),0.0f};
		float* ti_ptr = max_4(tmin);
		float* tf_ptr = min_4(tmax);
//		isMax[0] = (*ti_ptr == iMax[0]) || (*ti_ptr == iMax[1]) || (*ti_ptr == iMax[2]);
		__syncthreads();
		*ti = *ti_ptr;
		*tf = *tf_ptr;
		//trick to find starting dimension (pointer arithmatic):
		*startDim = ti_ptr - tmin;
	}


__device__ float siddonTrace_EXP(const float* ray, float alphaMin, float alphaMax, int startDim)
//      forward projection along the rayline
{
        
		float length = sqrt( ray[0]*ray[0]+ray[1]*ray[1]+ray[2]*ray[2] );
//      the total length from source to pixel
		
		float direction[3] = { (ray[0] < 0.0f)*(-2) + 1, (ray[1] < 0.0f)*(-2) + 1, (ray[2] < 0.0f)*(-2) + 1 };
		//float direction[3] = { float( (ray[0] < 0.0f)*(-2) + 1 ), float( (ray[1] < 0.0f)*(-2) + 1) , float( (ray[2] < 0.0f)*(-2) + 1)};
//      direction of the propagation

		float dAlpha[3] = { dc_voxSize[0] / abs(ray[0]), dc_voxSize[1] / abs(ray[1]), dc_voxSize[2] / abs(ray[2]) };
        
        float alpha = alphaMin;
        float alphax, alphay, alphaz;
        float ix,iy,iz;
//      variables used

        if(startDim == 0)
        {
                if(direction[0] == 1)
                        ix = 0.0f;
                else
                        ix = dc_voxDim[0] - 1;
                alphax = alpha + dAlpha[0];

                iy = float( int( (dc_source[1] + alpha*ray[1])/dc_voxSize[0] + dc_voxDim[1]/2 ) );
                alphay = ( (iy + (direction[1] > 0) - dc_voxDim[0]/2) * dc_voxSize[0] - dc_source[1] ) / ray[1];

                iz = float( int( (dc_source[2] + alpha*ray[2])/dc_voxSize[2] + dc_voxDim[2]/2 ) );
                alphaz = ( (iz + (direction[2] > 0) - dc_voxDim[2]/2) * dc_voxSize[2] - dc_source[2] ) / ray[2];
        }
        else if(startDim == 1)
        {
                if(direction[1] == 1)
                        iy = 0.0f;
                else
                        iy = dc_voxDim[0] - 1;
                alphay = alpha + dAlpha[1];

                ix = float( int( (dc_source[0] + alpha*ray[0])/dc_voxSize[0] + dc_voxDim[0]/2 ) );
                alphax = ( (ix + (direction[0] > 0) - dc_voxDim[0]/2) * dc_voxSize[0] - dc_source[0] ) / ray[0];

                iz = float( int( (dc_source[2] + alpha*ray[2])/dc_voxSize[2] + dc_voxDim[2]/2 ) );
                alphaz = ( (iz + (direction[2] > 0) - dc_voxDim[2]/2) * dc_voxSize[2] - dc_source[2] ) / ray[2];
        }
        else if(startDim == 2)
        {
                if(direction[2] == 1)
                        iz = 0.0f;
                else
                        iz = dc_voxDim[2] - 1;
                alphaz = alpha + dAlpha[2];

                ix = float( int( (dc_source[0] + alpha*ray[0])/dc_voxSize[0] + dc_voxDim[0]/2 ) );
                alphax = ( (ix + (direction[0] > 0) - dc_voxDim[0]/2) * dc_voxSize[0] - dc_source[0] ) / ray[0];

                iy = float( int( (dc_source[1] + alpha*ray[1])/dc_voxSize[0] + dc_voxDim[1]/2 ) );
                alphay = ( (iy + (direction[1] > 0) - dc_voxDim[0]/2) * dc_voxSize[0] - dc_source[1] ) / ray[1];
        }
//      initialize the ray tracing

        float result = 0.0F;
        while(alpha < alphaMax - DS_STRIDE_TOL)
//      tracing the line while accumulate the projection
        {
                float mu = tex3D(tex_vox,ix,iy,iz);
                if(alphax <= alphay && alphax <= alphaz)
                {
                        //result += length * (alphax-alpha) * mu;//tex1Dfetch( src_tex, ind3to1(ix, iy, iz, nx, nx, nz) );
						mu *= (alphax-alpha);
                        alpha = alphax;
                        alphax += dAlpha[0];
                        ix += direction[0];
                }
                else if(alphay <= alphaz)
                {
                        //result += length * (alphay-alpha) * mu;
						mu *= (alphay-alpha);
                        alpha = alphay;
                        alphay += dAlpha[1];
                        iy += direction[1];
                }
                else
                {
                        //result += length * (alphaz-alpha) * mu;
						mu *= (alphaz-alpha);
                        alpha = alphaz;
                        alphaz += dAlpha[2];
                        iz += direction[2];
                }
                result += mu;
        }

        return result*length;
//      return;
}


	/** the core Siddon ray trace function THIS IS BROKEN, _EXP works properly */
	__device__ inline float siddonTrace(const float* ray, float ti, float tf, int startDim){
		if(tf > ti){
			tf -= DS_STRIDE_TOL; //avoid over-run

			//compute direction sign:
			float direction[3];
			#pragma unroll
			for(int i=0; i<3; i++){
				direction[i] = float(ray[i] < 0.0f)*(-2.0f) + 1.0f;
			}
			//int startDim = d_startDim[idx];

			//starting point:
			float start[3];
			#pragma unroll
			for(int i=0;i<3;i++)
				start[i] = ray[i]*ti + dc_source[i];

			//step sizes:
			float sx = dc_voxSize[0]/fabs(ray[0]);
			float sy = dc_voxSize[1]/fabs(ray[1]);
			float sz = dc_voxSize[2]/fabs(ray[2]);

			//iterators:
			float ix,iy,iz; //tex3D takes floating point values as arguments
			float tx,ty,tz;

			//compute voxel indicies and parametric values:
			ix = floor( (start[0]-dc_voxPlanesMin[0]+DS_PLANE_IDX_TOL)/dc_voxSize[0] );
			iy = floor( (start[1]-dc_voxPlanesMin[1]+DS_PLANE_IDX_TOL)/dc_voxSize[1] );
			iz = floor( (start[2]-dc_voxPlanesMin[2]+DS_PLANE_IDX_TOL)/dc_voxSize[2] );
			tx = ( dc_voxPlanesMin[0] + (ix+float(direction[0]>0.0f))*dc_voxSize[0] - dc_source[0] ) / (ray[0]);
			ty = ( dc_voxPlanesMin[1] + (iy+float(direction[1]>0.0f))*dc_voxSize[1] - dc_source[1] ) / (ray[1]);
			tz = ( dc_voxPlanesMin[2] + (iz+float(direction[2]>0.0f))*dc_voxSize[2] - dc_source[2] ) / (ray[2]);
			//handle special cases:
			if(startDim == 0){       //starting on x boundary
				if(direction[0] < 0.0f){ ix = float(dc_voxDim[0] - 1); }else{ ix = 0.0f; }
				tx = ti + sx;
			}else if(startDim == 1){ //starting on y boundary
				if(direction[1] < 0.0f){ iy = float(dc_voxDim[1] - 1); }else{ iy = 0.0f; }
				ty = ti + sy;
			}else if(startDim == 2){ //starting on z boundary
				if(direction[2] < 0.0f){ iz = float(dc_voxDim[2] - 1); }else{ iz = 0.0f; }
				tz = ti + sz;
			}
			__syncthreads();//may not help much

			float pixIntensity = 0.0;
			float tlen, mu;
			float len = sqrt( pow(ray[0],2) + pow(ray[1],2) + pow(ray[2],2) );

			//The Incremental Siddon Loop:
			do{
				mu = tex3D(tex_vox,ix,iy,iz); //this is the current voxel
				if( tx<=ty && tx<=tz){        //Think about how to calc. # of intersections instead
					tlen = (tx - ti);
					//ti = tx;
					tx += sx;
					ix += direction[0];
				}else if( ty<=tz ){
					tlen= (ty - ti);
					//ti = ty;
					ty += sy;
					iy += direction[1];
				}else{
					tlen = (tz - ti);
					//ti = tz;
					tz += sz;
					iz += direction[2];
				}
				pixIntensity += tlen*mu;
				ti+=tlen;
			} while(ti < tf);
			__syncthreads();
			return pixIntensity*len;// [pixIntesity] == 1/mm
			
		}else{//if no intersection
			return 0.0f;
		}
	}

	///FIX: there might be something wrong with this fixed stride kernel...
	///     maybe not, the time seems right on large images
	__device__ inline float trilinearTrace(float* ray, float ti, float tf, int startDim){
		if(tf > ti){
			//starting point:
			float start[3];
			#pragma unroll
			for(int i=0;i<3;i++){
					start[i] = ray[i]*ti + dc_source[i];
					ray[i] *= (tf-ti);
			}
			float len = sqrt( pow(ray[0],2) + pow(ray[1],2) + pow(ray[2],2) );

			//step sizes step = intersectionLength/(samplingFactor*len) --- in voxel space:
			float factor = 1.0f/(dc_samplingFreq*len); // one divided by the numer of samples along the ray
			//dividing ray[*]/len gives unit vector
			
			float sx = ray[0]*factor/dc_voxSize[0];
			float sy = ray[1]*factor/dc_voxSize[1];
			float sz = ray[2]*factor/dc_voxSize[2];

			float ix,iy,iz;

			//find index for each dimension:
			ix = floor( (start[0]-dc_voxPlanesMin[0]+DS_PLANE_IDX_TOL)/dc_voxSize[0] );
			iy = floor( (start[1]-dc_voxPlanesMin[1]+DS_PLANE_IDX_TOL)/dc_voxSize[1] );
			iz = floor( (start[2]-dc_voxPlanesMin[2]+DS_PLANE_IDX_TOL)/dc_voxSize[2] );

			//handle special cases:
			if(startDim == 0){       //starting on x boundary
				if(sx < 0.0f){ ix = float(dc_voxDim[0]); }else{ ix = 0.0f; }
			}else if(startDim == 1){ //starting on y boundary
				if(sy < 0.0f){ iy = float(dc_voxDim[1]); }else{ iy = 0.0f; }
			}else if(startDim == 2){ //starting on z boundary
				if(sz < 0.0f){ iz = float(dc_voxDim[2]); }else{ iz = 0.0f; }
			}
			__syncthreads();//may not help much

			float pixIntensity = 0.0f;
			//for texture filtering to match up with voxel geometry
			ix+=0.5f;
			iy+=0.5f;
			iz+=0.5f;

			for(int l = int(floor(len*dc_samplingFreq)) ; l > 0.0f; l--){
				pixIntensity += tex3D(tex_vox,ix,iy,iz);
				ix+=sx;
				iy+=sy;
				iz+=sz;
			}
			return pixIntensity/dc_samplingFreq; // [pixIntesity] == 1/mm
		}//end if
		else{ //if no intersections
			return 0.0f;
		}
	}//end trilinear trace kernel

	/**
	 * Siddon DRR kernel with low/no global memory requirement
	 * ptxas info(sm20): Used 28 registers, 4+0 bytes lmem, 64 bytes cmem[0], 108 bytes cmem[2], 20 bytes cmem[16]
	 */
	__global__ void siddonDRR(
		float* pixPtX,
		float* pixPtY,
		float* pixPtZ,
		float* projection
	){
		float row = float(  blockIdx.x / dc_blocksPerRow );
		float col = float( (blockIdx.x % dc_blocksPerRow)*blockDim.x + threadIdx.x );

		float ray[3];
		int idx = blockIdx.x*blockDim.x + threadIdx.x;

		//Nieve bounds check:
		if(idx < dc_maxElement){
			if(dc_CBCTmode){
				#pragma unroll
				for(int i=0;i<3;i++){
					ray[i] = (dc_corner[i] + row*dc_pixelDown[i] + col*dc_pixelRight[i]) - dc_source[i] + DS_RAY_TOL;
				}
			}else{
				//compute ray from provided detector points
				ray[0] = pixPtX[idx] - dc_source[0] + DS_RAY_TOL;
				ray[1] = pixPtY[idx] - dc_source[1] + DS_RAY_TOL;
				ray[2] = pixPtZ[idx] - dc_source[2] + DS_RAY_TOL;
			}

			float ti, tf;
			int startDim;

			//siddonInitTraceParameters_EXP    (ray, &ti, &tf,  &startDim); //was NOT the problem
			siddonInitTraceParameters    (ray, &ti, &tf, &startDim);
			
			projection[idx] = siddonTrace_EXP(ray,  ti,  tf,  startDim); //uses 26 regs tot (WORKS!)
//			projection[idx] = siddonTrace(ray,  ti,  tf,  startDim); //uses 27 regs tot (BROKEN)

		}//bounds check
	}//end DRR kernel

	/**
	 * Siddon DRR kernel with low/no global memory requirement
	 * ptxas info(sm20): Used 28 registers, 4+0 bytes lmem, 64 bytes cmem[0], 108 bytes cmem[2], 20 bytes cmem[16]
	 */
	__global__ void trilinearDRR(
		float* pixPtX,
		float* pixPtY,
		float* pixPtZ,
		float* projection
	){
		float row = float(  blockIdx.x / dc_blocksPerRow );
		float col = float( (blockIdx.x % dc_blocksPerRow)*blockDim.x + threadIdx.x );

		float ray[3];
		int idx = blockIdx.x*blockDim.x + threadIdx.x;

		//Nieve bounds check:
		if(idx < dc_maxElement){
			if(dc_CBCTmode){
				#pragma unroll
				for(int i=0;i<3;i++){
					ray[i] = (dc_corner[i] + row*dc_pixelDown[i] + col*dc_pixelRight[i]) - dc_source[i] + DS_RAY_TOL;
				}
			}else{
				//compute ray from provided detector points
				ray[0] = pixPtX[idx] - dc_source[0] + DS_RAY_TOL;
				ray[1] = pixPtY[idx] - dc_source[1] + DS_RAY_TOL;
				ray[2] = pixPtZ[idx] - dc_source[2] + DS_RAY_TOL;
			}

			float ti, tf;
			int startDim, isMax;

			siddonInitTraceParameters       (ray, &ti, &tf,  &startDim);
			projection[idx] = trilinearTrace(ray,  ti,  tf,   startDim);

		}//bounds check
	}//end DRR kernel

__global__ void rotateVoxels3D(float sineTh, float cosineTh){
/*	int tid = blockIdx.x*blockDim.x+threadIdx.x;
	int ix = tid % dc_voxDim[0];
	int iy = (tid/dc_voxDim[1]) % dc_voxDim[1];

	//center of rotation:
	float centerX = float(dc_voxDim[0])/2.0f;//+ 0.5f; //i think the 0.5f should stay there?
	float centerY = float(dc_voxDim[1])/2.0f;//+ 0.5f; //i think the 0.5f should stay there?
	//new coords:
	float y = float(iy) - centerY;
	float x = float(ix) - centerX;
	//float X = (x*cosineTh -  y*sineTh ) + centerX;
	//float Y = ( x*sineTh  + y*cosineTh) + centerY;

	//patch corner in quilt space
	//int quiltX4 = ((blockIdx.x%QUILT_DIM_0)*dc_voxDim[1] + threadIdx.x)*4; //4 is required by api // quilt X (col)
	//int quiltY = (blockIdx.x/QUILT_DIM_0)*dc_voxDim[2]; //quilt Y (quiltRow)
	
	//int tidX4 = threadIdx.x*4; //this is required by API

	surf3Dwrite( tex3D(tex_vox,(x*cosineTh - y*sineTh  )+centerX,
	                           (x*sineTh   + y*cosineTh)+centerY,
	                           float(blockIdx.x)+0.5f),
	             transVox_surf3D,
	             iy,//y
	             blockIdx.x, //z
	             ix,//x
	             cudaBoundaryModeTrap
	           );
*/
}

__global__ void rotateVoxels(float sineTh, float cosineTh){

	int tid = blockIdx.x*blockDim.x+threadIdx.x;


	//center of rotation (in voxel index space):
	///todo: allow for arbitrary iso-center in x-y plane
	float centerX = float(dc_voxDim[0])/2.0f;
	float centerY = float(dc_voxDim[1])/2.0f;
	//new coords:
	float x = float(tid % dc_voxDim[0])  - centerX + 0.5f;
	float y = float(tid / dc_voxDim[1])  - centerY + 0.5f;
	float X = (x*cosineTh -  y*sineTh  ) + centerX;
	float Y = (x*sineTh   +  y*cosineTh) + centerY;


#if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ >= 200)
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID roatation kernel for CUDA ARCH >= 2.0
	//patch corner in 2D quilt space (surface)
	int quiltX4 = ((blockIdx.x%QUILT_DIM_0)*dc_voxDim[1] + threadIdx.x)*4; //4 is required by api // quilt X (col)
	int quiltY = (blockIdx.x/QUILT_DIM_0 /*quilt row*/)*dc_voxDim[2]; //quilt Y (row)
	surf2Dwrite( tex3D(tex_vox,X,Y,float(blockIdx.y)+0.5f), transVox_surf2D, quiltX4, quiltY+blockIdx.y,cudaBoundaryModeTrap);//added voxel shift here because of linear filtering
#endif

}


__global__ void rotateVoxelLayer(float sineTh, float cosineTh){//, int layer){

	//center of rotation (in voxel index space):
	///todo: allow for arbitrary iso-center in x-y plane
	float centerX = float(dc_voxDim[0])/2.0f;
	float centerY = float(dc_voxDim[1])/2.0f;
	int absX = blockDim.x*blockIdx.x + threadIdx.x;
	int absY = blockDim.y*blockIdx.y + threadIdx.y;
	//new coords:
	float x = float( absX ) - centerX + 0.5f;
	float y = float( absY )  - centerY + 0.5f;
	float X = (x*cosineTh -  y*sineTh  ) + centerX;
	float Y = (x*sineTh   +  y*cosineTh) + centerY;


#if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ >= 200)
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID roatation kernel for CUDA ARCH >= 2.0
	//patch corner in 2D quilt space (surface)
	//int quiltX4 = (absX + threadIdx.y*512 + (blockIdx.y%2)*512*16 )*4; //4 is required by api // quilt X (col)
	int quiltX4 = (absX + threadIdx.y*dc_voxDim[0] + (blockIdx.y%(QUILT_DIM_0/blockDim.y))*dc_voxDim[1]*blockDim.y )*4; //4 is required by api // quilt X (col)
	//int quiltY = (absY/32)*dc_voxDim[2]; //quilt Y (row) //
	int quiltY = (absY/QUILT_DIM_0)*dc_voxDim[2]; //quilt Y (row) //
	surf2Dwrite( tex3D(tex_vox,X,Y,float(blockIdx.z)+0.5f) , transVox_surf2D, quiltX4, quiltY+blockIdx.z,cudaBoundaryModeTrap);//added voxel shift here because of linear filtering
#endif

}




__global__ void rotateVoxels(float* rotatedVoxPtr, float sineTh, float cosineTh){
	int tid = blockIdx.x*blockDim.x+threadIdx.x;


	//center of rotation (in voxel index space):
	///todo: allow for arbitrary iso-center in x-y plane
	float centerX = float(dc_voxDim[0])/2.0f;
	float centerY = float(dc_voxDim[1])/2.0f;
	//new coords:
	float x = float(tid % dc_voxDim[0])  - centerX + 0.5f;
	float y = float(tid / dc_voxDim[1])  - centerY + 0.5f;
	float X = (x*cosineTh -  y*sineTh  ) + centerX;
	float Y = (x*sineTh   +  y*cosineTh) + centerY;

	int iy =  tid % dc_voxDim[0];
	int ix = (tid / dc_voxDim[1]);
	int iz = blockIdx.y;
	// transformation DxDy*z+Dx*y+x -> DyDx*x +Dy*z +y
	int tempIdx = dc_voxDim[1]*(dc_voxDim[0]*ix + iz) + iy;
	if(tempIdx < dc_voxDim[0]*dc_voxDim[1]*dc_voxDim[2])
        rotatedVoxPtr[dc_voxDim[1]*(dc_voxDim[0]*ix + iz) + iy] =  tex3D(tex_vox,X,Y,float(iz)+0.5f);//z gets +0.5 here
}


//rotation kernel
__global__ void //part of "old fixed grid"
preRotate(float* X, float* Y,float sineTh, float cosineTh){
	int tid = blockIdx.x*blockDim.x+threadIdx.x;
	int ix = tid % dc_voxDim[0];
	int iy = (tid/dc_voxDim[1]) % dc_voxDim[1];
	//int iz = float(tid/(dc_voxDim[0]*dc_voxDim[0]));
	float centerX = float(dc_voxDim[0])/2.0f;
	float centerY = float(dc_voxDim[1])/2.0f;
	float x = float(ix) - centerX + 0.5f;
	float y = float(iy) - centerY + 0.5f;
	X[tid] = (x*cosineTh -  y*sineTh ) + centerX;
	Y[tid] = ( x*sineTh  + y*cosineTh) + centerY; 
}

__global__ void rotateLayer(float* destination, float* X,float* Y){//,float z){
	int tid = blockIdx.x*blockDim.x+threadIdx.x;
	//int Xlayer = blockIdx.x;//*dc_voxDim[2]*dc_voxDim[0];

	//__shared__ float sX[512];
	//__shared__ float sY[512];
	
	float sX = X[tid];
	float sY = Y[tid];
//	float fZ = 0.5f;
	//__syncthreads();
	//destination[tid] = tex3D(tex_vox,X[tid],Y[tid],z);
	
	// maybe use shared memory here?

#if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ < 200)
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID roatation kernel for CUDA ARCH < 2.0
	for(int z = 0; z<dc_voxDim[2]; z++){
		//this could be wrong:
		dc_rotatedVoxData_ptr[ dc_voxDim[1]*(dc_voxDim[0]*blockIdx.x%QUILT_DIM_0 + z) +(blockIdx.x/QUILT_DIM_0)] =  tex3D(tex_vox,sX,sY,float(z)+0.5f);//z gets +0.5 here
	}
#else
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID roatation kernel for CUDA ARCH >= 2.0
	//patch corner in quilt space
	int quiltX4 = (blockIdx.x%QUILT_DIM_0)*dc_voxDim[1]*4; //4 is required by api // quilt X (col)
	int quiltY = (blockIdx.x/QUILT_DIM_0 /*quilt row*/)*dc_voxDim[2]; //quilt Y (row)
	int tidX4 = threadIdx.x*4; //this is required by API

	for(int z = 0; z<dc_voxDim[2]; z++){
		surf2Dwrite( tex3D(tex_vox,sX+0.5f,sY+0.5f,float(z)+0.5f), transVox_surf2D, quiltX4+tidX4, quiltY+z,cudaBoundaryModeTrap); //added voxel shift here because of linear filtering
	}
#endif

}

__global__ void renderAndFlatten( //got registers down to 19
	const int N,
	float* projection,
	const int angleIndex
){
	float row_Yincr = float(blockIdx.x / dc_blocksPerRow); // a reused variable
	float col_Zincr = float((blockIdx.x % dc_blocksPerRow)*blockDim.x + threadIdx.x); // a reused variable
	//int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	float rayX_len  = (dc_corner[0] + row_Yincr*dc_pixelDown[0] + col_Zincr*dc_pixelRight[0]) - dc_source[0] + DS_RAY_TOL; //a reused variable
	float rayY_maxY = (dc_corner[1] + row_Yincr*dc_pixelDown[1] + col_Zincr*dc_pixelRight[1]) - dc_source[1] + DS_RAY_TOL; //a reused variable
	float rayZ_maxZ = (dc_corner[2] + row_Yincr*dc_pixelDown[2] + col_Zincr*dc_pixelRight[2]) - dc_source[2] + DS_RAY_TOL; //a reused variable

	//parametric intersection with CENTER of first x-slab 
	float tx_pixI = ( dc_voxPlanesMax[0] - 0.5f*dc_voxSize[0] -dc_source[0] ) / rayX_len; //a reused variable 
	
	//starting points
	float Y = rayY_maxY*tx_pixI + dc_source[1];
	float Z = rayZ_maxZ*tx_pixI + dc_source[2];

	//parametric next x-layer
	//carefull here when layer averageing
//	float sy = rayY_maxY/rayX_len*dc_voxSize[0]; //added to preserve generality usually 
//	float sz = rayZ_maxZ/rayX_len*dc_voxSize[0];

	tx_pixI = (dc_voxPlanesMax[0] - 1.5f*dc_voxSize[0] - dc_source[0])/rayX_len;//carefull here when layer averageing 
	float sy = (rayY_maxY*tx_pixI + dc_source[1] - Y);// /dc_voxSize[1];
	float sz = (rayZ_maxZ*tx_pixI + dc_source[2] - Z);// /dc_voxSize[2];

	//intersection length is magnetude of "step" vector in real space
	rayX_len = sqrt( pow(dc_voxSize[0],2) + pow(sy,2) + pow(sz,2) );

	//convert to voxel index space:
	sy=sy/dc_voxSize[1];
	sz=sz/dc_voxSize[2];

	//need to shift to voxel index space, converting to units of voxels = cm/(cm/vox)
	Y = (Y-dc_voxPlanesMin[1])/dc_voxSize[1];// + 0.5f; // adding 0.5 for texture read
	Z = (Z-dc_voxPlanesMin[2])/dc_voxSize[2];// + 0.5f; // adding 0.5 for texture read
	///TODO: MOD ICBCT 1/15/2013 got rid of +0.5f;

	// trace vars
	tx_pixI = 0.0f;

#if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ < 200)
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID flattening kernel for CUDA ARCH < 2.0
	//use 3D texture method (assumes traversing along x-axis in negative direction
	for(float Xlayer = 0.5f; Xlayer<float(dc_voxDim[2]); Xlayer++){ // this will be different for layer averaging
		//tx_pixI += tex3D(tex_rot_vox3D,Y,Z,Xlayer);
		tx_pixI += tex3D(tex_rot_vox3D,Y,Z,Xlayer);
		Y+=sy;
		Z+=sz;
	}
#else
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID flattening kernel for CUDA ARCH >= 2.0
	rayY_maxY = dc_voxDim_f[1]*float(QUILT_DIM_0);
	rayZ_maxZ = dc_voxDim_f[2]*(dc_voxDim_f[0]/float(QUILT_DIM_0));
	row_Yincr = dc_voxDim_f[1];
	col_Zincr = dc_voxDim_f[2];

	//maybe try counting the expected number of intersections?
	for(float Zquilt=0.0f; Zquilt<rayZ_maxZ ; Zquilt+=col_Zincr){
		for( float Yquilt=0.0f;  Yquilt<rayY_maxY ; Yquilt+=row_Yincr){
			if(Y > 0.0f & Y < dc_voxDim_f[1] & Z > 0.0f & Z < dc_voxDim_f[2]){ //this should be floats!
				tx_pixI += tex2D(tex_rot_vox2D,Yquilt+Y,Zquilt+Z);
			}
			Y+=sy;
			Z+=sz;
		}
	}
#endif
	//save out
	projection[N*angleIndex+blockIdx.x*blockDim.x + threadIdx.x] = tx_pixI*rayX_len; //reused variable
}



__global__ void renderAndFlatten3( //got registers down to 19
	const int N,
	float* projection,
	const int angleIndex
){
	float row_Yincr =  blockDim.y*blockIdx.y+threadIdx.y;//float(blockIdx.x / dc_blocksPerRow); // a reused variable (row here)
	float col_Zincr =  blockDim.x*blockIdx.x+threadIdx.x;//float((blockIdx.x % dc_blocksPerRow)*blockDim.x + threadIdx.x); // a reused variable (col here)
	//int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	float rayX_len  = (dc_corner[0] + row_Yincr*dc_pixelDown[0] + col_Zincr*dc_pixelRight[0]) - dc_source[0] + DS_RAY_TOL; //a reused variable
	float rayY_maxY = (dc_corner[1] + row_Yincr*dc_pixelDown[1] + col_Zincr*dc_pixelRight[1]) - dc_source[1] + DS_RAY_TOL; //a reused variable
	float rayZ_maxZ = (dc_corner[2] + row_Yincr*dc_pixelDown[2] + col_Zincr*dc_pixelRight[2]) - dc_source[2] + DS_RAY_TOL; //a reused variable

	//parametric intersection with CENTER of first x-slab 
	float tx_pixI = ( dc_voxPlanesMax[0] - 0.5f*dc_voxSize[0] -dc_source[0] ) / rayX_len; //a reused variable 
	
	//starting points
	float Y = rayY_maxY*tx_pixI + dc_source[1];
	float Z = rayZ_maxZ*tx_pixI + dc_source[2];

	//parametric next x-layer
	//carefull here when layer averageing
//	float sy = rayY_maxY/rayX_len*dc_voxSize[0]; //added to preserve generality usually 
//	float sz = rayZ_maxZ/rayX_len*dc_voxSize[0];

	tx_pixI = (dc_voxPlanesMax[0] - 1.5f*dc_voxSize[0] - dc_source[0])/rayX_len;//carefull here when layer averageing 
	float sy = (rayY_maxY*tx_pixI + dc_source[1] - Y);// /dc_voxSize[1];
	float sz = (rayZ_maxZ*tx_pixI + dc_source[2] - Z);// /dc_voxSize[2];

	//intersection length is magnetude of "step" vector in real space
	rayX_len = sqrt( pow(dc_voxSize[0],2) + pow(sy,2) + pow(sz,2) );

	//convert to voxel index space:
	sy=sy/dc_voxSize[1];
	sz=sz/dc_voxSize[2];

	//need to shift to voxel index space, converting to units of voxels = cm/(cm/vox)
	Y = (Y-dc_voxPlanesMin[1])/dc_voxSize[1];// + 0.5f; // adding 0.5 for texture read
	Z = (Z-dc_voxPlanesMin[2])/dc_voxSize[2];// + 0.5f; // adding 0.5 for texture read
	///TODO: MOD ICBCT 1/15/2013 got rid of +0.5f;

	// trace vars
	tx_pixI = 0.0f;

#if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ < 200)
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID flattening kernel for CUDA ARCH < 2.0
	//use 3D texture method (assumes traversing along x-axis in negative direction
	for(float Xlayer = 0.5f; Xlayer<float(dc_voxDim[2]); Xlayer++){ // this will be different for layer averaging
		//tx_pixI += tex3D(tex_rot_vox3D,Y,Z,Xlayer);
		tx_pixI += tex3D(tex_rot_vox3D,Y,Z,Xlayer);
		Y+=sy;
		Z+=sz;
	}
#else
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID flattening kernel for CUDA ARCH >= 2.0
	rayY_maxY = dc_voxDim_f[1]*float(QUILT_DIM_0);
	rayZ_maxZ = dc_voxDim_f[2]*(dc_voxDim_f[0]/float(QUILT_DIM_0));
	row_Yincr = dc_voxDim_f[1];
	col_Zincr = dc_voxDim_f[2];

	//maybe try counting the expected number of intersections?
	for(float Zquilt=0.0f; Zquilt<rayZ_maxZ ; Zquilt+=col_Zincr){
		for( float Yquilt=0.0f;  Yquilt<rayY_maxY ; Yquilt+=row_Yincr){
			if(Y > 0.0f & Y < dc_voxDim_f[1] & Z > 0.0f & Z < dc_voxDim_f[2]){ //this should be floats!
				tx_pixI += tex2D(tex_rot_vox2D,Yquilt+Y,Zquilt+Z);
			}
			Y+=sy;
			Z+=sz;
		}
	}
#endif
	//save out
	projection[N*angleIndex + blockIdx.x*blockDim.x + threadIdx.x + blockDim.x*gridDim.x*(blockIdx.y*blockDim.y + threadIdx.y)] = tx_pixI*rayX_len; //reused variable
}


__global__ void renderAndFlatten2( //got registers down to 19
	const int N,
	float* projection,
	const int angleIndex
){
	float row_Yincr = blockDim.x*blockIdx.x+threadIdx.x;//float(blockIdx.x / dc_blocksPerRow); // a reused variable
	float col_Zincr = blockDim.y*blockIdx.y+threadIdx.y;//float((blockIdx.x % dc_blocksPerRow)*blockDim.x + threadIdx.x); // a reused variable
	//int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	float rayX_len  = (dc_corner[0] + row_Yincr*dc_pixelDown[0] + col_Zincr*dc_pixelRight[0]) - dc_source[0] + DS_RAY_TOL; //a reused variable
	float rayY_maxY = (dc_corner[1] + row_Yincr*dc_pixelDown[1] + col_Zincr*dc_pixelRight[1]) - dc_source[1] + DS_RAY_TOL; //a reused variable
	float rayZ_maxZ = (dc_corner[2] + row_Yincr*dc_pixelDown[2] + col_Zincr*dc_pixelRight[2]) - dc_source[2] + DS_RAY_TOL; //a reused variable

	//parametric intersection with CENTER of first x-slab 
	float tx_pixI = ( dc_voxPlanesMax[0] - 0.5f*dc_voxSize[0] -dc_source[0] ) / rayX_len; //a reused variable 
	
	//starting points
	float Y = rayY_maxY*tx_pixI + dc_source[1];
	float Z = rayZ_maxZ*tx_pixI + dc_source[2];

	//parametric next x-layer
	//carefull here when layer averageing
//	float sy = rayY_maxY/rayX_len*dc_voxSize[0]; //added to preserve generality usually 
//	float sz = rayZ_maxZ/rayX_len*dc_voxSize[0];

	tx_pixI = (dc_voxPlanesMax[0] - 1.5f*dc_voxSize[0] - dc_source[0])/rayX_len;//carefull here when layer averageing
	float sy = (rayY_maxY*tx_pixI + dc_source[1] - Y);// /dc_voxSize[1];
	float sz = (rayZ_maxZ*tx_pixI + dc_source[2] - Z);// /dc_voxSize[2];

	//intersection length is magnetude of "step" vector in real space
	rayX_len = sqrt( pow(dc_voxSize[0],2) + pow(sy,2) + pow(sz,2) );

	//convert to voxel index space:
	sy=sy/dc_voxSize[1];
	sz=sz/dc_voxSize[2];

	//need to shift to voxel index space, converting to units of voxels = cm/(cm/vox)
	Y = (Y-dc_voxPlanesMin[1])/dc_voxSize[1];// + 0.5f; // adding 0.5 for texture read
	Z = (Z-dc_voxPlanesMin[2])/dc_voxSize[2];// + 0.5f; // adding 0.5 for texture read
	///TODO: MOD ICBCT 2/14/2013 got rid of +0.5f;

	// trace vars
	tx_pixI = 0.0f;

#if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ < 200)
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID flattening kernel for CUDA ARCH < 2.0
	//use 3D texture method (assumes traversing along x-axis in negative direction
	for(float Xlayer = 0.5f; Xlayer<float(dc_voxDim[2]); Xlayer++){ // this will be different for layer averaging
		//tx_pixI += tex3D(tex_rot_vox3D,Y,Z,Xlayer);
		tx_pixI += tex3D(tex_rot_vox3D,Y,Z,Xlayer);
		Y+=sy;
		Z+=sz;
	}
#else
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID flattening kernel for CUDA ARCH >= 2.0
	rayY_maxY = dc_voxDim_f[1]*float(QUILT_DIM_0);
	rayZ_maxZ = dc_voxDim_f[2]*(dc_voxDim_f[0]/float(QUILT_DIM_0));
	row_Yincr = dc_voxDim_f[1];
	col_Zincr = dc_voxDim_f[2];

	//maybe try counting the expected number of intersections?
	for(float Zquilt=0.0f; Zquilt<rayZ_maxZ ; Zquilt+=col_Zincr){
		for( float Yquilt=0.0f;  Yquilt<rayY_maxY ; Yquilt+=row_Yincr){
			if(Y > 0.0f & Y < dc_voxDim_f[1] & Z > 0.0f & Z < dc_voxDim_f[2]){ //this should be floats!
				tx_pixI += tex2D(tex_rot_vox2D,Yquilt+Y,Zquilt+Z);
			}
			Y+=sy;
			Z+=sz;
		}
	}
#endif
	//save out
	projection[N*angleIndex+blockIdx.x*blockDim.x + threadIdx.x] = tx_pixI*rayX_len; //reused variable
}

//find the xplane intersection t value for the max plane
//save voxel values into ray
/* part of older Fixed grid method */
__global__ void renderDetector(
	float* d_ray,
	float* sy,
	float* sz,
	float* len,
	const int N)
{
	float row = float(blockIdx.x / dc_blocksPerRow);
	float col = float((blockIdx.x % dc_blocksPerRow)*blockDim.x + threadIdx.x);
	int idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	float ray[3],pixPt[3],tx;
	
	//ray is vector from source to pixel in units of cm
	for(int i=0;i<3;i++){
		pixPt[i] = dc_corner[i] + row*dc_pixelDown[i] + col*dc_pixelRight[i];
		ray[i] = pixPt[i] - dc_source[i] + DS_RAY_TOL;
	}

	//parametric intersection with first x-plane 
	tx = ( dc_voxPlanesMax[0] - dc_source[0] ) / ray[0]; ///took out slice thickness, we will center it upon retrieval instead

	//this is the layer intersection length for each pixel:
	//slice thickness devided by x componant of the ray unit vector
	len[idx] = (dc_voxPlanesMin[0]-dc_voxPlanesMax[0])/ray[0] //don't change this, it will always be positive!
				*sqrt( pow(ray[0],2) + pow(ray[1],2) + pow(ray[2],2) )
				/float(dc_voxDim[0]);//divide by half as many when layer averaging!

	//this is how far y and z move between each x layer in units of voxels
	//float norm = -(pixPt[0] - dc_source[0])/dc_voxSize[0];
	//sy[idx] = dc_voxSize[1]/fabs(ray[0]);//ray[1]/norm/dc_voxSize[1];
	//sz[idx] = dc_voxSize[2]/fabs(ray[0]);//ray[2]/norm/dc_voxSize[2];
	
	//find intersections with first x-plane
	float start[3];
	for(int i=0;i<3;i++){
		start[i] = ray[i]*tx + dc_source[i];
	}
	
	//next x-layer
	tx = (dc_voxPlanesMax[0]-dc_voxSize[0] - dc_source[0])/ray[0];//carefull here when layer averageing
	sy[idx] = (ray[1]*tx + dc_source[1] - start[1])/dc_voxSize[1];
	sz[idx] = (ray[2]*tx + dc_source[2] - start[2])/dc_voxSize[2];

	//need to shift to voxel index space, converting to units of voxels = cm/(cm/vox)
	start[1] = (start[1]-dc_voxPlanesMin[1])/dc_voxSize[1];
	start[2] = (start[2]-dc_voxPlanesMin[2])/dc_voxSize[2];

	//save out
	for(int i=1;i<3;i++){
		d_ray[idx] = start[i];
		idx += N;
	}
}

__global__ void flatten(
	const float* d_ray,
	const float* d_sy,
	const float* d_sz,
	float* len,
	const int N,
	float* projection,
	const int angleIndex)
{
	int idx = blockIdx.x*blockDim.x + threadIdx.x;

	//step sizes
	float sy = d_sy[idx];
	float sz = d_sz[idx];
	
	//initial values
	//float X = 0.0f; //not saved
	float Y = d_ray[idx]   + 0.5f; //ading shift for filtered texture read
	float Z = d_ray[idx+N] + 0.5f; //ading shift for filtered texture read
	
	//vars
	float pixIntensity = 0.0f;

//maybe try counting the expected number of intersections?
#if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ < 200)
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID flattening kernel for CUDA ARCH < 2.0
	//use 3D texture method (assumes traversing along x-axis in negative direction
	for(float Xlayer = 0.5f; Xlayer<float(dc_voxDim[2]); Xlayer++){ // this will be different for layer averaging
		pixIntensity += tex3D(tex_rot_vox3D,Y,Z,Xlayer);
		Y+=sy;
		Z+=sz;
	}
#else
#warning [DRR_SUITE_GLOBAL] compiling FIXED GRID flattening kernel for CUDA ARCH >= 2.0
	float maxY = float(dc_voxDim[1]*QUILT_DIM_0);
	float maxZ = float(dc_voxDim[2]*dc_voxDim[0]/QUILT_DIM_0);
	float Yincr = float(dc_voxDim[1]);
	float Zincr = float(dc_voxDim[2]);
	//surface technique uses 2D
		for(int Zquilt=0.0f; Zquilt<maxZ ; Zquilt+=Zincr){
			for( int Yquilt=0.0f;  Yquilt<maxY ; Yquilt+=Yincr){
				if(Y > 0.0f & Y < dc_voxDim[1] & Z > 0.0f & Z < dc_voxDim[2]){
					pixIntensity += tex2D(tex_rot_vox2D,Yquilt+Y,Zquilt+Z);
				}
				Y+=sy;
				Z+=sz;
			}
		}
	//need to add normalization?
#endif
	//save out
	projection[N*angleIndex+idx] = pixIntensity*len[idx]; //I think this len needs to be length of intersection!!
}


}//end DRR_SUITE_GLOBAL namespace

#endif
