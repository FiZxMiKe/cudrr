from ctypes import *
from math import pi as PI
from random import random

#link to the shared library WOOHOO!
#be sure to add path to the 'libdrrsuite.so.1' library in #LD_LIBRARY_PATH
ds = CDLL("lib/libdrrsuite.so.1.0.1")

#the cone beam CT configuration object
class CBCT(object):
    def __init__(self):
        self.obj = ds.cb_new()
        self._as_parameter_ = self.obj
        
    def __del__(self):
        ds.cb_delete(self.obj)

    def from_param(self):
        #check type?
        return self
        
    #wrapping
    def getN(self):
        return ds.cb_getN(self)

    def getArea(self):
        return ds.cb_getArea(self)

    def getAngle(self, idx):
        return ds.cb_getAngle(self,idx)*180.0/PI #in degrees
    
    def getNumAngles(self):
        return ds.cb_getNumAngles(self)
    
    def setAngles(self,num,start,delta):
        ds.cb_setAnglesDeg(self,num,start,delta)

    def setIsocenter(self,x,y,z):
        ds.cb_setIsocenter(self,x,y,z)

    def setShifts(self,rt_px,up_px):
        ds.cb_setShifts(self,rt_px,up_px)
 
    pass

#input types
#ds.cb_delete.argtypes = [CBCT]
ds.cb_getN.argtypes = [CBCT]
ds.cb_getArea.argtypes = [CBCT]
ds.cb_getAngle.argtypes = [CBCT,c_int]
ds.cb_setShifts.argtypes = [CBCT,c_float,c_float]
ds.cb_getNumAngles.argtypes = [CBCT]
ds.cb_setAnglesDeg.argtypes = [CBCT,c_int,c_float,c_float]
ds.cb_setIsocenter.argtypes = [CBCT,c_float,c_float,c_float]

#return types
ds.cb_getN.restype = c_int
ds.cb_getArea.restype = c_int
ds.cb_getAngle.restype = c_float
ds.cb_getNumAngles.restype = c_int
        
    
#the drr object
class DRR(object):
    
    def __init__(self,gpu=0,algorithm=0):
        # 0 = SIDDON, 1 = TRILINEAR, 2 = FIXEDGRID_OLD, 3 = FIXEDGRID_NEW, 4 = FIXEDGRID_REUSE};
        self.obj = ds.ds_new(gpu,algorithm)
        self._as_parameter_ = self.obj
        
    def __del__(self):
        ds.ds_delete(self.obj)
    
    def from_param(self):
        #type check?
        return self
        
    def getProjection(self,cbct):
        float_array_result = (c_float * cbct.getN())()
        ds.ds_getProjection(self,cbct,float_array_result)
        return float_array_result

    def getGPU(self):
        return ds.ds_getGPU(self)

    def setVoxels(self,phantom):
        #if type(phantom) == PHANTOM
        ds.ds_setVoxels(self,cast(phantom.dimensions,POINTER(c_int)),phantom.voxSize,phantom.array)

    pass

#input types
#ds.ds_delete.argtypes = [DRR]
ds.ds_new.argtypes = [c_int,c_int]
ds.ds_getProjection.argtypes = [DRR,CBCT,POINTER(c_float)]
ds.ds_getGPU.argtypes = [DRR]
ds.ds_setVoxels.argtypes = [DRR,POINTER(c_int),POINTER(c_float),POINTER(c_float)]

#return types
ds.ds_getGPU.restype = c_int 

class PHANTOM(object):
    
    def __init__(self,filename=None,dimensions=(128,128,128),voxSize=(2.5,2.5,2.5)):
        #setup default random phantom
        size_bytes = dimensions[0]*dimensions[1]*dimensions[2]
        self.dimensions = (c_int * 3)(int(dimensions[0]),int(dimensions[1]),int(dimensions[2]))
        self.voxSize = (c_float * 3)(voxSize[0],voxSize[1],voxSize[2])
        self.array = (c_float * size_bytes)() #(c_float * dimensions[0]*dimensions[1]*dimensions[2])
        if filename:
            fp = open(filename,'rb')
            import array
            temp_array = array.array('f')
            temp_array.fromfile(fp,dimensions[0]*dimensions[1]*dimensions[2])
            fp.close()
            for i in range(size_bytes):
                self.array[i] = temp_array[i]
        else:
            for i in range(size_bytes):
                self.array[i] = random()

