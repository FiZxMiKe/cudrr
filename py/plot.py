#! /usr/bin/env python

#author: Michael M. Folkerts (mmfolkerts@gmail.com)

import argparse
import sys
from array import array #need python-numeric
import numpy as np      #need python-numpy
import matplotlib       #need python-matplotlib

#parse input
parser = argparse.ArgumentParser(prog="plot.py",description='Commandline Plot Tool (saves .png by default)')
#files
parser.add_argument('-i', metavar='fileName', nargs='+', type=str, help='input file (as text)', dest='inFile')
parser.add_argument('-o', metavar='fileName', type=str, help='output file (prefix)',dest='outFile',   required=True)

#x axis
parser.add_argument('--Xrange',    nargs=2, metavar='<float>', type=float, help='set x-axis values (min max) - increment is inferred',default=None)
parser.add_argument('--Xincrement',nargs=2, metavar='<float>', type=float, help='set x-axis values (start increment) - max is inferred',default=None)

#labels
parser.add_argument('--Xlabel', metavar='<text>', type=str, help='set x-axis label',default=None)
parser.add_argument('--Ylabel', metavar='<text>', type=str, help='set y-axis label',default=None)
parser.add_argument('--Title',  metavar='<text>', type=str, help='set plot title',  default=None)

#legends
parser.add_argument('--Legends', nargs='*', metavar='<style string>', type=str, help='List plot styles in order',default=[])


#styles
parser.add_argument('--PlotStyles', nargs='*', metavar='<style string>', type=str, help='List plot styles in order',default=[])

#header
parser.add_argument('--Header',  metavar='N', type=int, help='indicate number of header lines in file',  default=None)

#file type
parser.add_argument('--PDF',action='store_true', help='additionally save to PDF (.pdf)',  default=False)
parser.add_argument('--EPS',action='store_true', help='additionally save to EPS (.eps)',  default=False)


#parser.add_argument('--Resolution', metavar='N', type=float, action='append', help='ignored (repeat for XYZ)')
#parser.add_argument('--Offset', metavar='N', type=float, action='append', help='ignored (repeat for XYZ)')

#movie switch
parser.add_argument('--Preview', action='store_true', help='shows preview of saved plot',default=False)

arg = parser.parse_args()
#print(arg)

#display check
if not arg.Preview:
	matplotlib.use('Agg')

import matplotlib.pyplot as plt


#sanity check

#only one x-axis type
if arg.Xrange is not None and arg.Xincrement is not None:
	print "ERROR: Please select only one x-axis value setting method\n"
	exit
	
#only one file type
#if arg.PDF is not None and arg.EPS is not None:
#	print "ERROR: Please select only one file type\n"
#	exit


import csv

data = []
fileCount = int(0)

for fileName in arg.inFile:
	tempCols = []
	#open input file
	inF = open(fileName,'r')

	parcer = csv.reader(inF,delimiter=',')
	if arg.Header is not None:
		#skip/ignore header lines
		for i in range(arg.Header):
			parcer.next()

	for row in parcer:
		#print len(row)
		for col in range(len(row)):
			#print col
			try:
				tempCols[col].append(float(row[col])) # append each value in each column to a column in data
			except Exception:
					tempCols.append([])
					tempCols[col].append(float(row[col]))
	data.append(tempCols)
	fileCount += 1

#print(data)


# plotting
fig = None
plt.hold(True)

for dataSet in range(fileCount):
	if len(data[dataSet]) == 1:
		#simple plot
		if arg.Xrange is not None:
			#print "range mode"
			start = arg.Xrange[0]
			step = (arg.Xrange[1] - start)/float(numPoints) # assumes increasing
			last = arg.Xrange[1]
			print (np.arange(start,last,step).__len__() )
			fig = plt.plot(np.arange(start,last,step),data,'ob')

		elif arg.Xincrement is not None:
			#print "increment mode"
			start = arg.Xincrement[0]
			step = arg.Xincrement[1]
			last = start + step*float(numPoints)
			print (np.arange(start,last,step).__len__())
			fig = plt.plot(np.arange(start,last,step),data,'ob')
		else:
			fig = plt.plot(data,'ob')
		print 'mean col 2 = {1}\n'.format( sum(data[1])/len(data[1]) )
	else:
		#multi plot
		for col in range(len(data[dataSet])-1):
			if len(arg.PlotStyles) > dataSet+col:
				fig = plt.plot(data[dataSet][0],data[dataSet][col+1],arg.PlotStyles[dataSet+col])
			else:
				fig = plt.plot(data[dataSet][0],data[dataSet][col+1],'ob')
			print 'mean of col {0} = {1}\n'.format(col+1, sum(data[dataSet][col+1])/len(data[dataSet][col+1]))



#resolution = 100
#widthInches = float(arg.Dimensions[0])/float(resolution)
#heightInches = float(arg.Dimensions[1])/float(resolution)
#fig = plt.figure( figsize=(widthInches, heightInches) )

leg = plt.legend(arg.Legends,loc=10)# 'upper center', shadow=True

# labels
if arg.Ylabel is not None:
	plt.ylabel(arg.Ylabel)

if arg.Xlabel is not None:
	plt.xlabel(arg.Xlabel)

if arg.Title is not None:
	plt.title(arg.Title)

# save file
if arg.PDF is not None:
	plt.savefig(arg.outFile+".pdf",format='pdf')#,transparent=True)
	print("figure saved: %s.pdf\n" % (arg.outFile) )
if arg.EPS is not None:
	plt.savefig(arg.outFile+".eps",format='eps')#,transparent=True)
	print("figure saved: %s.eps\n" % (arg.outFile) )

# default
plt.savefig(arg.outFile+".png",format='png')#,transparent=True)
print("figure saved: %s.png\n" % (arg.outFile) )



if arg.Preview:
	plt.show()

plt.close()
del fig

print "DONE!\n"
