#! /usr/bin/env python

#author: Michael M. Folkerts (mmfolkerts@gmail.com)

import argparse
import sys
from array import array #need python-numeric
import numpy as np      #need python-numpy
import matplotlib       #need python-matplotlib
# uncomment next line to not use Xdisplay
matplotlib.use('Agg')
import matplotlib.pyplot as plt

#parse input
parser = argparse.ArgumentParser(prog="calcNRMSD",description='Calculate Normalized RMS Deviation for 3D data, layer by layer\nsqrt( sum( square(diffLayer) ) / nElements ) / ( layerMax - layerMin )')
#files
parser.add_argument('-i', nargs=2, metavar='fileName', type=str, help='input files (2 required)',dest='inFile',required=True)
parser.add_argument('-o', metavar='fileName', type=str, help='output file (for plot data)',dest='outFile',required=True)
#some arguments passed by .xargs files
parser.add_argument('--Dimensions',nargs=3, metavar='N', type=int, help='Dimensionss of data (repeat for XYZ)',required=True)
#parser.add_argument('--Resolution', metavar='N', type=float, action='append', help='ignored (repeat for XYZ)')
#parser.add_argument('--Offset', metavar='N', type=float, action='append', help='ignored (repeat for XYZ)')

#movie switch
parser.add_argument('--PNGprefix', metavar='path/prefix', help='To save PNG files of diffLayers')

arg = parser.parse_args()
#print(arg)


def saveFigure(imdata, index, path_prefix='temp_img',Vmin=None,Vmax=None): 
	resolution = 100
	widthInches = float(arg.Dimensions[0])/float(resolution)
	heightInches = float(arg.Dimensions[1])/float(resolution)

	#print (widthInches)
	#print (heightInches)
	
	fig = plt.figure( figsize=(widthInches, heightInches) )
	plt.imshow(imdata,vmin=Vmin,vmax=Vmax)
	
	cb = plt.colorbar()
	cb.set_label('diff. / proj. mean')
	
	plt.axis('off')
	plt.axis('image')
	plt.savefig(path_prefix+str(index)+".png",format='png',dpi=resolution)#,transparent=True)

	sys.stdout.write("    figure saved: %s%i.png" % (path_prefix,index) )
	sys.stdout.flush()
	#plt.show()
	plt.close()
	del fig

#program constants
nElements = arg.Dimensions[0]*arg.Dimensions[1]

#print(arg.Dimensions[0])

#open our file (first command line argument)
inFile1 = open(arg.inFile[0],'rb')
inFile2 = open(arg.inFile[1],'rb')
outFile = open(arg.outFile,'w')
#plot data
norm_RMSDeviation = []

# temp float array declaration
dataArray1 = array('f')
dataArray2 = array('f')

#this loads binary data directly into an array:
dataArray1.fromfile(inFile1,nElements*arg.Dimensions[2])
dataArray2.fromfile(inFile2,nElements*arg.Dimensions[2])


dataDiff = np.subtract( np.array(dataArray1,copy=False,order='C'), \
	                    np.array(dataArray2,copy=False,order='C')  )
diffMin = dataDiff.min()
diffMax = dataDiff.max()

# process slice by slice
for i in range(arg.Dimensions[2]):
	#user feedback
	sys.stdout.write('\rProcessing Layer: %i'%(i))
	sys.stdout.flush()
	
	dataFrame = dataDiff[i*nElements:(i+1)*nElements]
	
	layerMin = (np.array(dataArray1[i*nElements:(i+1)*nElements],copy=False,order='C')).min()
	layerMax = (np.array(dataArray1[i*nElements:(i+1)*nElements],copy=False,order='C')).max()
	layerMean = (np.array(dataArray1[i*nElements:(i+1)*nElements],copy=False,order='C')).mean()
	
	if arg.PNGprefix:
		saveFigure(dataFrame.reshape(arg.Dimensions[1],arg.Dimensions[0])/layerMean, i,path_prefix=arg.PNGprefix)# Vmin=layerMin/layerMean, Vmax=layerMax/layerMean)

	norm_RMSDeviation.append(  np.sqrt( np.square(dataFrame).sum() / nElements ) / ( layerMax - layerMin )  )
	outFile.write("%f\n" % (norm_RMSDeviation[i]))

	del dataFrame

print "DONE!"
