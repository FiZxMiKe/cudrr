APP_NAME = drrSuiteTest
APP_CODE = drrSuiteTest.cpp

LIB_NAME = drrsuite
CU_CODE  = drrSuite.cu
CU_DEPS  = drrSuite_global.cu
#used for library building only
MAJ_REV = 1
MIN_REV = .0.1

# put your custom cuda path here
# default:
CUDA_ROOT = /usr/local/cuda/


#adjust this to match your compute version:
#code=sm_XX get hard-coded into binary at compile time (limited compatability)
#code=compute_XX get compiled at run-time for a given model (future compatable)
ifndef CUDA_ARCH
#	CUDA_ARCH = -gencode arch=compute_20,code=sm_21
#	CUDA_ARCH = -gencode arch=compute_10,code=sm_11

CUDA_ARCH = -arch=sm_35
#	CUDA_ARCH = -gencode arch=compute_20,code=compute_20
endif

#NVCC_FLAGS = --keep --keep-dir=PTX

all:experiment

# example of how to build from source
fromSource:
	$(CUDA_ROOT)/bin/nvcc $(APP_CODE) drrSuite.cu $(CUDA_ARCH) -Xptxas -v -o $(APP_NAME)_fromSource.x

# experimental executable
experiment:
	$(CUDA_ROOT)/bin/nvcc drrExperiment.cpp drrSuite.cu -o drrSuite_experiment.x -Xptxas -v $(NVCC_FLAGS) $(CUDA_ARCH) \
	--compiler-options "-ansi -largtable2" --linker-options "-largtable2"


experiment_debug:
	nvcc drrExperiment.cpp drrSuite.cu -g -G $(CUDA_ARCH) -Xptxas "-v -g" -Xlinker "-g" -Xcompiler "-g -ansi -largtable2" -o drrSuite_exp_debug.x


# example of how to combile a static library
staticLib:
	mkdir -p obj
	mkdir -p lib
	nvcc -c $(CU_CODE) $(CUDA_ARCH) -Xptxas -v -o ./obj/$(LIB_NAME).src.o
	#todo:make this handle more than one extra file!
	#nvcc -c $(CU_DEPS) -o ./obj/$(LIB_NAME).dep.o

	ar rcs ./lib/lib$(LIB_NAME).a ./obj/$(LIB_NAME).src.o
	chmod +x ./lib/libdrrsuite.a

# example of how to build from dynamically linked libraries
sharedLib:
	mkdir -p obj
	mkdir -p lib
	nvcc -c -Xcompiler "-fpic -DPIC" $(CU_CODE) $(CUDA_ARCH) -Xptxas -v -o ./obj/$(LIB_NAME).o -DPYTHON_LIB=1
	g++ -shared -Wl,-soname,lib$(LIB_NAME).so.$(MAJ_REV) -o ./lib/lib$(LIB_NAME).so.$(MAJ_REV)$(MIN_REV) ./obj/$(LIB_NAME).o -L/usr/local/cuda/lib64 -lcudart -lcuda 
	#note the library dependencies at the end for proper functionality (with python)

libLinks:
	#cover our bases for linking and execution
	#cd lib
	#ln -sf lib$(LIB_NAME).so.$(MAJ_REV)$(MIN_REV) lib$(LIB_NAME).so
	#ln -sf lib$(LIB_NAME).so.$(MAJ_REV)$(MIN_REV) lib$(LIB_NAME).so.$(MAJ_REV)
    

# after making the (LIB_NAME).a file, you can compile.

fromLib:
    # You must export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/lib
	gcc -static $(APP_CODE) -o $(APP_NAME)_fromLib.x -I./lib -I/usr/local/cuda/include -L./lib -L/usr/local/cuda/lib64 -L/usr/local/cuda/lib -lcudart -lcuda -l$(LIB_NAME)


jpg:
	bin2jpg -i single_projection.floats -x 512 -y 512 -z 1 -o single_projection

# to make a video name append extention use 'make vid ext=<extension>'
vid640:
ifndef LAYERS
		echo "Please append LAYERS=<num-of-layers> to your make command"
else
	mkdir -p png
	
	bin2jpg -i cbct_projections.floats --PNG --GlobalNorm -x 640 -y 480 -z $(LAYERS) -o png/cbct_projection_
	#generate an mpeg movie with same quality as input
	ffmpeg -y -f image2 -r 10 -sameq -i png/cbct_projection_%d.png cbct_video$(ext).mp4
	rm -rf png
endif

vidEcono:
	mkdir -p png
	bin2jpg -i cbct_projections.floats --PNG --GlobalNorm -x 1024 -y 768 -z 100 -o png/cbct_projection_
	ffmpeg -f image2 -sameq -r 10 -i png/cbct_projection_%d.png cbct_econo_video.mp4
	rm -rf png

# for testing use 'make test_<res> a=<alg>'
test_512:
	 ./drrSuite_experiment.x dat/input512.txt -a$(a)

#test_512_debug:
#	gdb --args ./drrSuite_exp_debug.x dat/input512.txt -a2


installLib:
	ln -sf `pwd`/lib/libdrrsuite.so.1.0.1 $(HOME)/lib/libdrrsuite.so.1
	ln -sf `pwd`/lib/libdrrsuite.a $(HOME)/lib/libdrrsuite.a


#clean up
clean:
	rm -rf $(APP_NAME)_* lib obj
