//Author: Michael Folkerts, mmfolkerts@gmail.com, http://bit.ly/folkerts
//Start Date: March 2012
#ifndef DRR_SUITE_SOURCE
#define DRR_SUITE_SOURCE


#include "drrSuite.cuh"
#include <stdio.h>
#include <string.h>
#include <algorithm>
using std::min_element;

#define DS_RAY_TOL	   1e-15f 
#define DS_STRIDE_TOL	1e-15f
#define DS_PLANE_IDX_TOL 1e-15f
#define DS_GPU_THREADS   128
//#define DS_GPU_THREADS   256

//FIXED GRID STUFF
#define QUILT_DIM_0 32
//#define QUILT_DIM_1 32
#define NO_FLATTEN false

//REUSE PARAMS
// #define REUSE_DELTA 2.5
#define REUSE_DELTA 10.0


//TESTING STUFF
#define NUM_RUNS 20

#define NEW_ROTATE true


cudaError_t drrSuite_TempErrorCode;
//a DRR Suite (DS) error checking macro
#define DS_ERROR_CHECK(e) drrSuite_TempErrorCode = e; \
	if(drrSuite_TempErrorCode) fprintf(stderr,"DRR SUITE CUDA ERROR [%i] in %s at line %d : %s\n",drrSuite_TempErrorCode,__FILE__,__LINE__,cudaGetErrorString(drrSuite_TempErrorCode)); \


// include various required Constants, Cuda Kernels and Textures:
#include "drrSuite_global.cu"


/** A function for all common constructor actions */
void drrSuite::construct(){
	voxelsSet = false;
	verbose = true;
	dryRun = false;
	economy = false;
//	dataOutputOnly = false; //replaced by timing mode

	ROTATION_REUSE_MODE=false; //must set through member variable!
	timingMode=false;
	for(int i=0; i<3;i++){ //causes floting point exception in FIXEDGRID if zero
		voxDim[i] = 0;
		voxSize[i] = 0.0f;
	}
	DS_ERROR_CHECK( cudaSetDevice(gpuID) );
	DS_ERROR_CHECK( cudaGetDeviceProperties(&gpuInfo,gpuID) );
}

/** default constructor */
drrSuite::drrSuite(){
	//default trace kind is Siddon's algorithm
	traceType = SIDDON;
	gpuID = 0;
	construct(); //to handle all common constructor actions
}

/** algorithm/GPUID constructor
 * for now, the algorithm type can only be set at creation time to avoid
 * complications with cleanup and initilization that is algorithm dependent
 * same goes for the GPU ID, we dont want to manage switching GPUs
 */
drrSuite::drrSuite(
	const int gID,				 /**< [in] GPU device number to be used */
	drrSuite_algorithm_t algorithm /**< [in] set the ray tracing algorithm to be used */
){
	traceType = algorithm;
	gpuID = gID;
	construct(); // to handle all common constructor actions
}

drrSuite::~drrSuite(){
	//clean up host pointers
	//free(volDimension_host);
	//free(voxSize_host);

	//clean house
	if(timingMode){ //only when set
		delete timingFileName;
	}
	

	//clean up GPU
	//unbind texture before freeing array else seg. fault
	DS_ERROR_CHECK( cudaUnbindTexture(DRR_SUITE_GLOBAL::tex_vox) );

	DS_ERROR_CHECK( cudaFreeArray(volArray_dev) );

}

int drrSuite::getGPU(){
	DS_ERROR_CHECK( cudaGetDeviceProperties(&gpuInfo,gpuID) ); //update info
	printf("GPU INFO:\n");
	printf("\tgpuID: %d\n",gpuID);
	printf("\tname: %s\n",gpuInfo.name);
	printf("\tcompute capability: %d.%d\n",gpuInfo.major,gpuInfo.minor);
	printf("\ttotalGlobalMem(MiB): %d\n",(int)gpuInfo.totalGlobalMem/(1<<20));
	printf("\tmultiProcessorCount: %d\n",gpuInfo.multiProcessorCount);
	printf("\tclockRate(MHz): %.2f\n",float(gpuInfo.clockRate)/1000.0);
	printf("\tkernelExecTimeoutEnabled: %s\n",gpuInfo.kernelExecTimeoutEnabled?"yes":"no");
	
	DS_ERROR_CHECK( cudaGetDevice(&gpuID) );
	return gpuID;
};

void drrSuite::resetGPU(){
	DS_ERROR_CHECK( cudaDeviceReset(); );
}

/** set timing mode to true and set output file path */
void drrSuite::setTimingMode(const char* fileName){
	int length = strlen(fileName);
	timingFileName = new char[length+20]; //leave room for null terminator and then some
	strcpy(timingFileName,fileName);
	timingMode = true;
}


bool drrSuite::setVoxels(
	const int*   volDim_in,   /**< [in] a 3-vector defining the dimension of the volume array  */
	const float* voxSize_in,  /**< [in] a 3-vector defining the size of all volume elements	*/
	const float* volArray,  /**< [in] an array containing the values for each volume element */
	bool usingDevicePointer
){
	namespace G = DRR_SUITE_GLOBAL;
	G::initTimer();
	G::startTimer();

	//Set volume information on host and dev
	for(int i=0; i<3;i++){
		voxDim[i] = volDim_in[i];
		voxSize[i] = voxSize_in[i];
	}

	///TODO: fix problem with setting device
	DS_ERROR_CHECK( cudaSetDevice(gpuID) );
	
	//Siddon's algorithm, the trilinear algorithm, and fixed grid have same texture copy parameters
	//if(traceType == SIDDON || traceType == TRILINEAR || traceType == FIXEDGRID){

		// if voxels have been previously set:
		if(voxelsSet){
			if(verbose && !timingMode) printf("DRR SUITE: Resetting Voxels\n");
			DS_ERROR_CHECK( cudaUnbindTexture(DRR_SUITE_GLOBAL::tex_vox) );
			DS_ERROR_CHECK( cudaFreeArray(volArray_dev) );
		}

		cudaExtent volumeSize = make_cudaExtent(voxDim[0], voxDim[1], voxDim[2]);
		volArray_dev = 0;

		// create 3D array
		cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();
		DS_ERROR_CHECK( cudaMalloc3DArray(&volArray_dev, &channelDesc, volumeSize) );

		// copy data to 3D array
		cudaMemcpy3DParms copyParams = {0};
		copyParams.srcPtr   = make_cudaPitchedPtr( (void*)volArray, volumeSize.width*sizeof(float), volumeSize.width, volumeSize.height );
		copyParams.dstArray = volArray_dev;
		copyParams.extent   = volumeSize;
		
		//change copy parameters when copying from dev to dev
		if(usingDevicePointer){
			copyParams.kind = cudaMemcpyDeviceToDevice;
		}else{
			copyParams.kind = cudaMemcpyHostToDevice;
		}

		DS_ERROR_CHECK( cudaMemcpy3D( &copyParams ) );

		// set texture parameters
		G::tex_vox.normalized = false; //do not access with normalized texture coordinates

		///TODO: make this fool proof for developers
		if(traceType !=SIDDON){ //interpolating methods
			//set texture filtering mode to linear interpolation
			G::tex_vox.filterMode = cudaFilterModeLinear;
			if(verbose && !timingMode) printf("DRR SUITE: Texture linear Interpolation Set.\n");
		}else{
			//use point access
			G::tex_vox.filterMode = cudaFilterModePoint;
		}
		// clamp texture edges for out of boundary access
		G::tex_vox.addressMode[0] = cudaAddressModeClamp;
		G::tex_vox.addressMode[1] = cudaAddressModeClamp;
		G::tex_vox.addressMode[2] = cudaAddressModeClamp;

		// bind array to 3D texture
		DS_ERROR_CHECK( cudaBindTextureToArray(G::tex_vox, volArray_dev, channelDesc) );

		if(traceType == TRILINEAR){
			samplingFreq = 1.0f / (*(min_element(voxSize,voxSize+3)));
			if(verbose && !timingMode) printf("DRR SUITE TRILINEAR: Sampling frequency automatically set to %.2f 1/mm\n",samplingFreq);
		} 
	//}//END TRILINEAR AND SIDDON voxel texture setup

	float voxDim_f[3];
	for(int i=0;i<3;i++){
		voxDim_f[i] = float(voxDim[i]);
	}

	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_voxDim_f, voxDim_f, 3*sizeof(float) ) ); 
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_voxDim,   voxDim,   3*sizeof(int)   ) );
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_voxSize,  voxSize,  3*sizeof(float) ) );
	
	voxelsSet = true;

	G::stopTimer();
	//if(verbose && !timingMode) printf("DRR SUITE: Total Set Voxels Time is %.2f ms\n",G::readTimer()); 
	G::cleanupTimer();
	return voxelsSet; //on success
}

void drrSuite::setVoxelPlanes(const float* volOffset){
		float voxPlanesMin_host[3];
		float voxPlanesMax_host[3];

		//set data boundary planes
		for(int i=0;i<3;i++){
			voxPlanesMin_host[i] = - volOffset[i];
			voxPlanesMax_host[i] = - volOffset[i] + voxSize[i] * voxDim[i];
		}

		//set projection information on host and device
		DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_voxPlanesMin,	voxPlanesMin_host,	3*sizeof(float)) );
		DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_voxPlanesMax,	voxPlanesMax_host,	3*sizeof(float)) );
}

void drrSuite::setCBCTgeoFromAxis(ds_CBCT* CBCT, float* projectionAxis){
	#pragma unroll
	for(int i=0;i<3;i++)
		source[i]= CBCT->getSAD()*projectionAxis[i];//vector from origin to source

	float detector[3];
	#pragma unroll
	for(int i=0;i<3;i++)
		detector[i] = -(CBCT->getSID()-CBCT->getSAD())*projectionAxis[i]; //vector from origin to CENTER of detector

	//-----define pixel vectors-----
	float pixHeight = CBCT->getHeight()/float(CBCT->getRows());
	float pixWidth  = CBCT->getWidth()/float(CBCT->getCols());

	float up[] = {0.0f,0.0f,1.0f};
	float pixelUp[3];
	#pragma unroll
	for(int i=0;i<3;i++)
		pixelUp[i] = -pixHeight*up[i]; //points up, length of pixel //UP IS DOWN FOR ICBCT CODE!! FIX BEFORE RELEASE

	float pixelLeft[3]; //vector pointing left, parallel to detector (beams eye view)
	//cross product:
	pixelLeft[0] = pixWidth*(projectionAxis[1]*up[2] - projectionAxis[2]*up[1]);
	pixelLeft[1] = pixWidth*(projectionAxis[2]*up[0] - projectionAxis[0]*up[2]);
	pixelLeft[2] = pixWidth*(projectionAxis[0]*up[1] - projectionAxis[1]*up[0]);

	//define upper lefthand corner of detector:
	///TODO: think about what happens when there is an odd number of pixels in either dimension
	#pragma unroll
	for(int i=0;i<3;i++) //we add 1/2 a pixel to left shift and subtract 1/2 a pixel from up direction to get center of pixel
		corner[i] = detector[i] + ( float(CBCT->getCols())/2.0f - 0.5f + CBCT->getShiftRt() )*pixelLeft[i] + ( float(CBCT->getRows())/2.0f - 0.5f + CBCT->getShiftUp() )*pixelUp[i];
	
//		corner[i] = detector[i] + ( (CBCT->getCols() - 1.0f + 2.0f*CBCT->getShiftRt())*pixelLeft[i] + (CBCT->getRows() - 1.0f + 2.0f*CBCT->getShiftUp())*pixelUp[i] )/2.0f;

	//define incremental vectors:
	#pragma unroll
	for(int i=0;i<3;i++){
		pixelRight[i] = -pixelLeft[i];
		pixelDown[i]  = -pixelUp[i];
	}

	//copy constants:
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_source,	 source,	 3*sizeof(float)) );
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_corner,	 corner,	 3*sizeof(float)) );
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_pixelRight, pixelRight, 3*sizeof(float)) );
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_pixelDown,  pixelDown,  3*sizeof(float)) );
}

/** overloaded to take the pre-computed source, corner, and detector unit vectors */
void drrSuite::setArbCBCTgeometry(ds_CBCT* CBCT, int th){
	//copy constants:
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_source,	 CBCT->source +th*3,	 3*sizeof(float)) );
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_corner,	 CBCT->corner +th*3,	 3*sizeof(float)) );
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_pixelRight, CBCT->pixelRight +th*3, 3*sizeof(float)) );
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_pixelDown,  CBCT->pixelDown +th*3,  3*sizeof(float)) );
}

/** overloaded to take the integer index to the angle array stored in CBCT config object */
void drrSuite::setCBCTgeometry( ds_CBCT* CBCT, int th){
	//-----define room vectors-----
	float projectionAxis[] = {cos(CBCT->getAngle(th)),sin(CBCT->getAngle(th)),0.0}; //vector pointing at source

	setCBCTgeoFromAxis(CBCT, projectionAxis);
}

/** overloaded to take an actual <float> angle (in radians) */
void drrSuite::setCBCTgeometry( ds_CBCT* CBCT, float th){
	//-----define room vectors-----
	float projectionAxis[] = {cos(th),sin(th),0.0}; //vector pointing at source

	setCBCTgeoFromAxis(CBCT, projectionAxis);
}


/**FIXEDGRID_NEW**/
bool drrSuite::getProjectionFixedGrid_DEV(
/* will only support CBCT object, not suitable for arbitrary geometry */
			  float* traceArray,	/**< [out] RESULT: array containing the result of the ray traces will have length endPtsLength (device pointer option) */
			  bool   useDevPtrs,	/**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
			  bool   useCBCTmode,
			  ds_CBCT* CBCT
	){

	DS_ERROR_CHECK( cudaSetDevice(gpuID) );
	int area;
	area = CBCT->getArea();

	setVoxelPlanes(CBCT->getIsocenterPtr());

	//shortcut to custom namespace
	namespace G = DRR_SUITE_GLOBAL;

	G::initTimer();
	G::startTimer();
	FILE* timingFile;

	//device pointers:
	float *d_rotatedVoxData;
	float *d_projection;


	//result array
	if(!useDevPtrs){
		DS_ERROR_CHECK(  cudaMalloc( (void**)&d_projection, sizeof(float)*CBCT->getN())  );
	}else{
		d_projection = traceArray;
	}
	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();

	cudaArray* dArray_rotatedVoxels=0;

	//cudaArray* d_rot_volumeArray = 0;
	cudaExtent rotVolumeSize;
	cudaMemcpy3DParms rotCopyParams = {0};
	
	//allow backwards compatability at runtime
	if(gpuInfo.major < 2){
		printf("[DRR SUITE WARNING] FIXED GRID: Inferior hardware detected: using falback texture copy method\n");

		/*--3Dtexture: support for devices that do not support texture writing -but why are you using this algorithm?-)--*/
		//temp array
		DS_ERROR_CHECK(  cudaMalloc( (void**)&d_rotatedVoxData, sizeof(float)*voxDim[0]*voxDim[1]*voxDim[2] )  );

		float* temp[1];
		temp[0] = d_rotatedVoxData;
		//need to set this address in constant memory to get backwards compatability to work (HACK!)
		DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_rotatedVoxData_ptr, temp,  sizeof(float*)) );

		/// todo check for enough ram first...
		rotVolumeSize = make_cudaExtent( voxDim[1], voxDim[2], voxDim[0]);//new format for voxels
		// create 3D array
		DS_ERROR_CHECK(  cudaMalloc3DArray( &dArray_rotatedVoxels, &channelDesc, rotVolumeSize )  );
		//settings to copy rotated voxels into cuda array
		rotCopyParams.srcPtr   = make_cudaPitchedPtr( (void*)d_rotatedVoxData, rotVolumeSize.width*sizeof(float), rotVolumeSize.width, rotVolumeSize.height );
		rotCopyParams.dstArray = dArray_rotatedVoxels;
		rotCopyParams.extent   = rotVolumeSize;
		rotCopyParams.kind	 = cudaMemcpyDeviceToDevice;

		DS_ERROR_CHECK(  cudaBindTextureToArray(DRR_SUITE_GLOBAL::tex_rot_vox3D, dArray_rotatedVoxels, channelDesc )  );

	}else{ //use 2D surface method
		
		/* surface quilt was 16x32 patches of 512x128 (512 patches total for X layers)
		 * DS_ERROR_CHECK(  cudaMallocArray(&dArray_rotatedVoxels, &channelDesc, 8192, 128*QUILT_DIM_1, cudaArraySurfaceLoadStore) );
		 * quiltDim[0] = /MAX_TEXTURE_DIM/512 = 128
		 * quiltDim[1] = /MAX_TEXTURE_DIM/128 = 512
		 */
		//#define MAX_TEXTURE_DIM 65536
		/// TODO: add sanity check for 2D texture use : gpuInfo.maxTexture2D[0,1]

		DS_ERROR_CHECK(  cudaMallocArray(&dArray_rotatedVoxels, &channelDesc,
			voxDim[1]*QUILT_DIM_0, voxDim[2]*(voxDim[0]/QUILT_DIM_0),
			cudaArraySurfaceLoadStore) );
		//cudaBindSurfaceToArray(transVox_surf, dArray_rotatedVoxels, &channelSurfDesc);

		//allow backwards compatablility at compile time:
		#if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ < 200)
		#warning skipping cudaBindSurfaceToArray for compute capability < 2.0
		#else
		DS_ERROR_CHECK(  cudaBindSurfaceToArray(DRR_SUITE_GLOBAL::transVox_surf2D, dArray_rotatedVoxels)  );
		#endif

		//TEXTURE SETTINGS:
		// set texture parameters
		G::tex_rot_vox2D.normalized = false;// access with normalized texture coordinates
		G::tex_rot_vox2D.filterMode = cudaFilterModeLinear;// linear interpolation
		G::tex_rot_vox2D.addressMode[0] = cudaAddressModeClamp;// wrap texture coordinates
		G::tex_rot_vox2D.addressMode[1] = cudaAddressModeClamp;

		// bind textures to arrays
		DS_ERROR_CHECK(  cudaBindTextureToArray(DRR_SUITE_GLOBAL::tex_rot_vox2D, dArray_rotatedVoxels, channelDesc )  );
	}
	
	///FIX: case where number of columns is not evenly divisible by blockDim
	blocksPerRow = 2;// CBCT->getCols()/1024;//set by hand for now
	
	///TODO: investigate better data/thread locality?
	//set the blocks-per-row parameter
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_blocksPerRow, &blocksPerRow,  sizeof(int)) );

	//thread setup for rotation kernel
	int layerVoxCount = voxDim[1]*voxDim[0]; //number of voxels in each layer layer of CT
	int rThreads = voxDim[1]; // patchWidth so long as this is not larger than maxThreads 512 (1024 fermi)
	int rBlocks = layerVoxCount/rThreads; //total number of blocks needed for a layer
	dim3 threadsPerBlock(voxDim[0]);
	dim3 blocksPerGrid(rBlocks,voxDim[2]);
	
	//thread setup for flatenning kernel
	const int THREADS = CBCT->getCols()/blocksPerRow;//BLOCKS_PER_ROW;
	const int NUM_BLOCKS = CBCT->getRows()*blocksPerRow;//BLOCKS_PER_ROW;
	
	
	///todo: make this member variable set by setFixedGridReuseMode()
	float reuseDelta = REUSE_DELTA*G::PI/180.0; //degrees -> radians


	//new kernel configurations
	//for roatation kernel
	dim3 new_grid(voxDim[0]/16,voxDim[1]/16,voxDim[2]);
	dim3 new_block(16,16);
	//for projection kernel
	dim3 grid2(CBCT->getCols()/16,CBCT->getRows()/16);
	dim3 block2(16,16);


	//in reuse mode the rotation of the data set should start at th_vox=th[0]+delta
	float thetaVox = CBCT->getAngle(0)+reuseDelta;
	if(ROTATION_REUSE_MODE){// rotate only once every 2*reuseDelta
		printf("DRR SUITE FIXEDGRID: [ROTATION REUSE MODE] Delta Theta set to %f deg.",reuseDelta*180.0/G::PI);
		if(gpuInfo.major>=2){
			//old //G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));//,float(z*512)+0.5f);
			G::rotateVoxelLayer<<<new_grid,new_block>>> (sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));//new
			
			DS_ERROR_CHECK(  cudaThreadSynchronize()  );
		}else{//backwards compatability
			G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (d_rotatedVoxData,sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));//,float(z*512)+0.5f);
			DS_ERROR_CHECK(  cudaThreadSynchronize()  );
			DS_ERROR_CHECK( cudaMemcpy3D(&rotCopyParams) );
		}
		printf(".");
		fflush(stdout);
		//printf("DRR SUITE FIXEDGRID: [ROTATION REUSE MODE] Rotated Voxels to %f deg.\n",thetaVox*180.0/G::PI);
	}else{//the projection angle is always "zero"
		setCBCTgeometry(CBCT, 0.0f);
	}

	if(timingMode){
		//open timing file for output
		timingFile = fopen(timingFileName,"w");
		fprintf(timingFile,"[FIXED GRID ALGORITHM | BEST OF %i KERNEL RUN TIMES]\nAngle (deg), Rotation (ms), Flatten (ms)\n----------------------------------------\n",NUM_RUNS);
	}
	
	
	//________Start Angle Loop___________
	float sineTh,cosineTh;
	for( int th=0; th<CBCT->getNumAngles(); th++){

		///might have to chnage dryRun logic
		if(!dryRun && !timingMode){ //no warmup when in timing mode

			if(ROTATION_REUSE_MODE){
				float newTh = CBCT->getAngle(th)-thetaVox;
				//Every subsequent rotation should happen when th[i]-thetaVox > delta
				if(newTh > reuseDelta){
					thetaVox = CBCT->getAngle(th)+reuseDelta;
					if(gpuInfo.major>=2){
						//old //G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));//,float(z*512)+0.5f);
						G::rotateVoxelLayer<<<new_grid,new_block>>> (sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));//,float(z*512)+0.5f);
						DS_ERROR_CHECK(  cudaThreadSynchronize()  );
					}else{//backwards compatability
						G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (d_rotatedVoxData,sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));//,float(z*512)+0.5f);
						DS_ERROR_CHECK(  cudaThreadSynchronize()  );
						DS_ERROR_CHECK( cudaMemcpy3D(&rotCopyParams) );
					}
//					printf("DRR SUITE FIXEDGRID: [ROTATION REUSE MODE] Rotated Voxels to %f deg.\n",thetaVox*180.0/G::PI);
					printf(".");
					setCBCTgeometry(CBCT,-reuseDelta);
				}else{
					setCBCTgeometry(CBCT,newTh);
				}
			}else{//else voxels will be rotated accordingly for each angle:
				cosineTh = cos(CBCT->getAngle(th)+G::PI/2.0f);
				sineTh = sin(CBCT->getAngle(th)+G::PI/2.0f);
			}

			if(!ROTATION_REUSE_MODE){ //rotate only when not reusing rotations
				if(gpuInfo.major>=2){
					//if(NEW_ROTATE){
					//	dim3 Grid(32,32);
					//	dim3 Block(16,16);
					//	for( int z = 0; z<voxDim[2]; z++){
					//		G::rotateVoxelLayer<<<Grid,Block>>> (sineTh,cosineTh,z);
					//	}
					//}else{
						//old //G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (sineTh,cosineTh);//,float(z*512)+0.5f);
						G::rotateVoxelLayer<<<new_grid,new_block>>> (sineTh,cosineTh);//,float(z*512)+0.5f);
					//}
					DS_ERROR_CHECK(  cudaThreadSynchronize()  );
				}else{//backwards compatability
					G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (d_rotatedVoxData,sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));//,float(z*512)+0.5f);
					DS_ERROR_CHECK(  cudaThreadSynchronize()  );
					DS_ERROR_CHECK( cudaMemcpy3D(&rotCopyParams) );
				}
			}

			//flattening algorithm
			if(!NO_FLATTEN){
				//G::renderDetector<<< NUM_BLOCKS, THREADS >>> (d_ray, d_sy, d_sz, d_len, area);
				//G::flatten<<< NUM_BLOCKS, THREADS >>>		(d_ray, d_sy, d_sz, d_len, area, d_projection, th);
				//old //G::renderAndFlatten<<< NUM_BLOCKS, THREADS >>> (area, d_projection, th);
				G::renderAndFlatten3<<< grid2, block2 >>> (area, d_projection, th);
				DS_ERROR_CHECK(cudaThreadSynchronize());
			}

			DS_ERROR_CHECK(cudaGetLastError());
		}

		if(!dryRun && timingMode){
			fprintf(timingFile,"%3.2f,",CBCT->getAngle(th)*180.0f/G::PI);
			
			//create a vector to store run times
			float* timings = new float[NUM_RUNS];

			//initialize cuda event timers
			cudaEvent_t start_event, stop_event;
			int eventflags = cudaEventBlockingSync;
			DS_ERROR_CHECK( cudaEventCreateWithFlags(&start_event, eventflags) );
			DS_ERROR_CHECK( cudaEventCreateWithFlags(&stop_event,  eventflags) );


			if(ROTATION_REUSE_MODE){
				float newTh = CBCT->getAngle(th)-thetaVox;
				
				//Every subsequent rotation should happen when th[i]-th_vox > delta
				if(newTh > reuseDelta){
					thetaVox = CBCT->getAngle(th)+reuseDelta;

					//time rotation kernel
					//#pragma unroll
					
//						dim3 new_grid(32,32,128);

//						dim3 new_grid(voxDim[0]/16,voxDim[1]/16,voxDim[2]);
//						dim3 new_block(16,16);
						//moved above up-scope
//						dim3 new_block(16,16);
						
					for(int i=0; i<NUM_RUNS; i++){
						//start a cuda timer
						DS_ERROR_CHECK(cudaEventRecord(start_event, 0));

//						if(gpuInfo.major>=2){

						//for( int z = 0; z<voxDim[2]; z++){

							G::rotateVoxelLayer<<<new_grid,new_block>>> (sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));

							//G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));

							//,float(z*512)+0.5f);
//						}else{//backwards compatability
//							G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (d_rotatedVoxData,sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));//,float(z*512)+0.5f);
//							DS_ERROR_CHECK(  cudaThreadSynchronize()  );
//							DS_ERROR_CHECK( cudaMemcpy3D(&rotCopyParams) );
//						}
						DS_ERROR_CHECK(cudaThreadSynchronize());
						DS_ERROR_CHECK(cudaEventRecord(stop_event, 0));
						DS_ERROR_CHECK(cudaEventSynchronize(stop_event));
						DS_ERROR_CHECK(cudaEventElapsedTime(timings+i, start_event, stop_event));
						DS_ERROR_CHECK(cudaThreadSynchronize());
					}
					//RECORD TIME
					fprintf(timingFile,"%.4f,",*(min_element(timings,timings+NUM_RUNS)));
					
					//printf("DRR SUITE FIXEDGRID: [ROTATION REUSE MODE] Rotated Voxels to %f deg.\n",thetaVox*180.0/G::PI);
					printf(".");
					fflush(stdout);
					setCBCTgeometry(CBCT,-reuseDelta);
				
				}else{
					//RECORD NO TIME (NO ROTATION AT THIS ANGLE)
					fprintf(timingFile,"0.00,");
					setCBCTgeometry(CBCT,newTh);
				}
			}else{//NO REUSE MODE : voxels will be rotated accordingly for each angle:
				
				cosineTh = cos(CBCT->getAngle(th)+G::PI/2.0f);
				sineTh = sin(CBCT->getAngle(th)+G::PI/2.0f);
				
				//time rotation kernel
				//#pragma unroll
				for(int i=0; i<NUM_RUNS; i++){
					//start a cuda timer
					DS_ERROR_CHECK(cudaEventRecord(start_event, 0));

//					if(gpuInfo.major>=2){


					//if(NEW_ROTATE){
//						dim3 new_grid(32,32,128);

//						dim3 new_grid(voxDim[0]/16,voxDim[1]/16,voxDim[2]);
//						dim3 new_block(16,16);
						//moved above up-scope
//						dim3 new_block(16,16);
						//for( int z = 0; z<voxDim[2]; z++){
							G::rotateVoxelLayer<<<new_grid,new_block>>> (sineTh,cosineTh);//,z); //new kernel ??
						//}
					//}else{
						//G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (sineTh,cosineTh);//,float(z*512)+0.5f); //old kernel
					
					//	G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (sineTh,cosineTh);
					//}


					//,float(z*512)+0.5f);
//					}else{//backwards compatability
//						G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (d_rotatedVoxData, sin(G::PI/2.0f+thetaVox),cos(G::PI/2.0f+thetaVox));//,float(z*512)+0.5f);
//						DS_ERROR_CHECK( cudaMemcpy3D(&rotCopyParams) );
//					}

//					if(gpuInfo.major < 2){
//						//copy rotated voxels into cuda array and rebind texture
//						DS_ERROR_CHECK(  cudaMemcpy3D( &rotCopyParams )  );
//					}

					//stop and record
						DS_ERROR_CHECK(cudaThreadSynchronize());
						DS_ERROR_CHECK(cudaEventRecord(stop_event, 0));
						DS_ERROR_CHECK(cudaEventSynchronize(stop_event));
						DS_ERROR_CHECK(cudaEventElapsedTime(timings+i, start_event, stop_event));
						//printf("timing: %f",timings[i]);
						DS_ERROR_CHECK(cudaThreadSynchronize());
				}
				//RECORD TIME
				fprintf(timingFile,"%.4f,",*(min_element(timings,timings+NUM_RUNS)));
			}
			
			//time renderAndFlatten kernel
			//#pragma unroll
			//dim3 grid2(CBCT->getCols()/16,CBCT->getRows()/16);
			//dim3 block2(16,16);
			//moved above up-scope
			
			for(int i=0; i<NUM_RUNS; i++){
				//start a cuda timer
				DS_ERROR_CHECK(cudaEventRecord(start_event, 0));

				//G::renderDetector<<< NUM_BLOCKS, THREADS >>> (d_ray, d_sy, d_sz, d_len, area);
				//G::flatten<<< NUM_BLOCKS, THREADS >>>		(d_ray, d_sy, d_sz, d_len, area, d_projection, th);

				//G::renderAndFlatten<<< NUM_BLOCKS, THREADS >>> (area, d_projection, th); //old kernel works
				
				G::renderAndFlatten3<<< grid2, block2 >>> (area, d_projection, th);//testing
				//G::renderAndFlatten2<<< grid2, block2 >>> (area, d_projection, th);

				//stop and record
				DS_ERROR_CHECK(cudaThreadSynchronize());
				DS_ERROR_CHECK(cudaEventRecord(stop_event, 0));
				DS_ERROR_CHECK(cudaEventSynchronize(stop_event));
				DS_ERROR_CHECK(cudaEventElapsedTime(timings+i, start_event, stop_event));
				DS_ERROR_CHECK(cudaThreadSynchronize());
				//if(th==0){
				//	fprintf(stdout,"angle zero,%i:%f\n",i,timings[i]);
				//}
			}
			fprintf(timingFile,"%.4f\n",*(min_element(timings,timings+NUM_RUNS)));
			//cleanup timers
			DS_ERROR_CHECK(cudaEventDestroy(start_event));
			DS_ERROR_CHECK(cudaEventDestroy(stop_event));
		}//-----end timing block-----//



	}//-----end angle loop-----

	if(ROTATION_REUSE_MODE){
		printf("\n");
	}

	if(!useDevPtrs){
		DS_ERROR_CHECK(  cudaMemcpy( traceArray, d_projection, sizeof(float)*CBCT->getN(), cudaMemcpyDeviceToHost )  );
		//cleanup
		DS_ERROR_CHECK( cudaFree(d_projection) );
	}
	
	// cleanup...
	
	if(timingMode){
		fclose(timingFile);
	}
	//FREE SURFACE?
	if( gpuInfo.major < 2){
		DS_ERROR_CHECK( cudaFree(d_rotatedVoxData) );
	}
	
	DS_ERROR_CHECK( cudaUnbindTexture(G::tex_rot_vox2D) );
	DS_ERROR_CHECK( cudaFreeArray(dArray_rotatedVoxels) );
	DS_ERROR_CHECK( cudaGetLastError() );

	G::stopTimer();
	if(verbose /*&& !timingMode*/) printf("DRR SUITE %s: Total %s Projection Time is %.2f ms\n","FIXEDGRID",dryRun?"Dry-Run":"Effective", G::readTimer()); 
	G::cleanupTimer();

	return false;//no error
}


/** depricated, also uses a more GPU ram (no dev pointer support) **/
bool drrSuite::getProjectionFixedGrid_DEV_OLD(
/* will only support CBCT object, not suitable for arbitrary geometry */
			  float* traceArray,	/**< [out] RESULT: array containing the result of the ray traces will have length endPtsLength (device pointer option) */
			  bool   useDevPtrs,	/**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
			  bool   useCBCTmode,
			  ds_CBCT* CBCT
	){

	DS_ERROR_CHECK( cudaSetDevice(gpuID) );
	int area;
	area = CBCT->getArea();

	setVoxelPlanes(CBCT->getIsocenterPtr());

	//shortcut to custom namespace
	namespace G = DRR_SUITE_GLOBAL;

	G::initTimer();
	G::startTimer();
	FILE* timingFile;

	//device pointers:
	float *d_rotatedVoxData;
	float *d_X;
	float *d_Y;
	float *d_projection;
	float *d_ray;
	float *d_sy;
	float *d_sz;
	float *d_len;

	//device pointers for rotation
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_X, sizeof(float)*voxDim[0]*voxDim[1] )  );
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_Y, sizeof(float)*voxDim[0]*voxDim[1] )  );

	//device pointers for projection
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_ray, 3*sizeof(float)*area) );
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_sy,	sizeof(float)*area) );
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_sz,	sizeof(float)*area) );
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_len,   sizeof(float)*area) );

	//result array
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_projection, sizeof(float)*CBCT->getN())  );
	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();

	cudaArray* dArray_rotatedVoxels=0;

	//cudaArray* d_rot_volumeArray = 0;
	cudaExtent rotVolumeSize;
	cudaMemcpy3DParms rotCopyParams = {0};
	if(gpuInfo.major < 2){
		printf("[DRR SUITE WARNING] FIXED GRID: Inferior hardware detected: using falback texture copy method\n");

		/*--3Dtexture: support for devices that do not support texture writing -but why are you using this algorithm?-)--*/
		//temp array
		DS_ERROR_CHECK(  cudaMalloc( (void**)&d_rotatedVoxData, sizeof(float)*voxDim[0]*voxDim[1]*voxDim[2] )  );

		//need to set this address in constant memory to get backwards compatability to work (HACK!)
		DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_rotatedVoxData_ptr, &d_rotatedVoxData,  sizeof(float*)) );

		/// todo check for enough ram first...
		rotVolumeSize = make_cudaExtent( voxDim[1], voxDim[2], voxDim[0]);//new format for voxels
		// create 3D array
		DS_ERROR_CHECK(  cudaMalloc3DArray( &dArray_rotatedVoxels, &channelDesc, rotVolumeSize )  );
		//settings to copy rotated voxels into cuda array
		rotCopyParams.srcPtr   = make_cudaPitchedPtr( (void*)d_rotatedVoxData, rotVolumeSize.width*sizeof(float), rotVolumeSize.width, rotVolumeSize.height );
		rotCopyParams.dstArray = dArray_rotatedVoxels;
		rotCopyParams.extent   = rotVolumeSize;
		rotCopyParams.kind	 = cudaMemcpyDeviceToDevice;

		DS_ERROR_CHECK(  cudaBindTextureToArray(DRR_SUITE_GLOBAL::tex_rot_vox3D, dArray_rotatedVoxels, channelDesc )  );

	}else{ //use 2D surface method
		//create 2D array

		/* surface quilt was 16x32 patches of 512x128 (512 patches total for X layers)
		 * DS_ERROR_CHECK(  cudaMallocArray(&dArray_rotatedVoxels, &channelDesc, 8192, 128*QUILT_DIM_1, cudaArraySurfaceLoadStore) );
		 * quiltDim[0] = /MAX_TEXTURE_DIM/512 = 128
		 * quiltDim[1] = /MAX_TEXTURE_DIM/128 = 512
		 */
		//#define MAX_TEXTURE_DIM 65536
		/// TODO: add sanity check for 2D texture use : gpuInfo.maxTexture2D[0,1]

		DS_ERROR_CHECK(  cudaMallocArray(&dArray_rotatedVoxels, &channelDesc, voxDim[1]*QUILT_DIM_0, voxDim[2]*(voxDim[0]/QUILT_DIM_0), cudaArraySurfaceLoadStore) );
		//cudaBindSurfaceToArray(transVox_surf, dArray_rotatedVoxels, &channelSurfDesc);

		#if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ < 200)
		#warning skipping cudaBindSurfaceToArray for compute capability < 2.0
		#else
		DS_ERROR_CHECK(  cudaBindSurfaceToArray(DRR_SUITE_GLOBAL::transVox_surf2D, dArray_rotatedVoxels)  );
		#endif

		//TEXTURE SETTINGS:
		// set texture parameters
		G::tex_rot_vox2D.normalized = false;// access with normalized texture coordinates
		G::tex_rot_vox2D.filterMode = cudaFilterModeLinear;// linear interpolation
		G::tex_rot_vox2D.addressMode[0] = cudaAddressModeClamp;// wrap texture coordinates
		G::tex_rot_vox2D.addressMode[1] = cudaAddressModeClamp;
//		G::tex_rot_vox2D.addressMode[2] = cudaAddressModeWrap; //it's only 2D

		// bind textures to arrays
		DS_ERROR_CHECK(  cudaBindTextureToArray(DRR_SUITE_GLOBAL::tex_rot_vox2D, dArray_rotatedVoxels, channelDesc )  );
	}
	///FIX: case where number of columns is not evenly divisible by blockDim
	blocksPerRow = 2;// CBCT->getCols()/1024;//set by hand for now
	
	///TODO: investigate better data/thread locality?
	//set the blocks-per-row parameter
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_blocksPerRow, &blocksPerRow,  sizeof(int)) );

	//thread setup for rotation kernel
	int layerVoxCount = voxDim[1]*voxDim[0]; //number of voxels in each layer layer of CT
	int rThreads = voxDim[1]; // patchWidth so long as this is not larger than maxThreads 512 (1024 fermi)
	int rBlocks = layerVoxCount/rThreads; //total number of blocks needed for a layer
	dim3 threadsPerBlock(voxDim[0]);
	dim3 blocksPerGrid(rBlocks,voxDim[2]);
	
	//thread setup for flatenning kernel
	const int THREADS = CBCT->getCols()/blocksPerRow;//BLOCKS_PER_ROW;
	const int NUM_BLOCKS = CBCT->getRows()*blocksPerRow;//BLOCKS_PER_ROW;
	
	if(ROTATION_REUSE_MODE){// rotate only once
		G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (sin(G::PI/2.0f),cos(G::PI/2.0f));//,float(z*512)+0.5f);
		DS_ERROR_CHECK(  cudaThreadSynchronize()  );
	}else{//the projection angle is always "zero"
		setCBCTgeometry(CBCT, 0.0f);
	}

	if(timingMode){
		//open timing file for output
		timingFile = fopen(timingFileName,"w");
		fprintf(timingFile,"[FIXED GRID ALGORITHM | BEST OF %i KERNEL RUN TIMES]\nAngle (deg), Rotation (ms), Flatten (ms)\n----------------------------------------\n",NUM_RUNS);
	}
	
	//-----Start Angle Loop-----
	for( int th=0; th<CBCT->getNumAngles(); th++){
		if(timingMode){
			fprintf(timingFile,"%3.2f,",CBCT->getAngle(th)*180.0f/G::PI);
		}

		if(ROTATION_REUSE_MODE){
			setCBCTgeometry(CBCT,th); // move projection angle
		}
		//else voxels will be rotated accordingly:
		float cosineTh = cos(CBCT->getAngle(th)+G::PI/2.0f);
		float sineTh = sin(CBCT->getAngle(th)+G::PI/2.0f);

		if(!dryRun){ //also warmup when in timing mode

			if(!ROTATION_REUSE_MODE){ //rotate only when not reusing rotations
				G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (sineTh,cosineTh);//,float(z*512)+0.5f);
				DS_ERROR_CHECK(  cudaThreadSynchronize()  );
			}

			if(gpuInfo.major < 2){
				//copy rotated voxels into cuda array and rebind texture
				DS_ERROR_CHECK(  cudaMemcpy3D( & rotCopyParams )  );
			}

			//flattening algorithm
			if(!NO_FLATTEN){
				G::renderDetector<<< NUM_BLOCKS, THREADS >>> (d_ray, d_sy, d_sz, d_len, area);
				G::flatten<<< NUM_BLOCKS, THREADS >>>		(d_ray, d_sy, d_sz, d_len, area, d_projection, th);
				//G::renderAndFlatten<<< NUM_BLOCKS, THREADS >>> (area, d_projection, th); the newer DEV kernel has this!
				DS_ERROR_CHECK(cudaThreadSynchronize());
			}

			DS_ERROR_CHECK(cudaGetLastError());
		}

		if(!dryRun && timingMode){
			//create a vector to store run times
			float* timings = new float[NUM_RUNS];

			//initialize cuda event timers
			cudaEvent_t start_event, stop_event;
			int eventflags = cudaEventBlockingSync;
			DS_ERROR_CHECK( cudaEventCreateWithFlags(&start_event, eventflags) );
			DS_ERROR_CHECK( cudaEventCreateWithFlags(&stop_event,  eventflags) );

			//time rotation kernel
			#pragma unroll
			for(int i=0; i<NUM_RUNS; i++){
				//start a cuda timer
				DS_ERROR_CHECK(cudaEventRecord(start_event, 0));

				G::rotateVoxels<<<blocksPerGrid, threadsPerBlock>>> (sineTh,cosineTh);//,float(z*512)+0.5f);
				DS_ERROR_CHECK(cudaThreadSynchronize());

				if(gpuInfo.major < 2){
					//copy rotated voxels into cuda array and rebind texture
					DS_ERROR_CHECK(  cudaMemcpy3D( &rotCopyParams )  );
				}

				//stop and record
				DS_ERROR_CHECK(cudaEventRecord(stop_event, 0));
				DS_ERROR_CHECK(cudaEventSynchronize(stop_event));
				DS_ERROR_CHECK(cudaEventElapsedTime(timings+i, start_event, stop_event));
			}
			fprintf(timingFile,"%.4f,",*(min_element(timings,timings+NUM_RUNS)));

			//time renderAndFlatten kernel
			#pragma unroll
			for(int i=0; i<NUM_RUNS; i++){
				//start a cuda timer
				DS_ERROR_CHECK(cudaEventRecord(start_event, 0));

				G::renderDetector<<< NUM_BLOCKS, THREADS >>> (d_ray, d_sy, d_sz, d_len, area);
				G::flatten<<< NUM_BLOCKS, THREADS >>>		(d_ray, d_sy, d_sz, d_len, area, d_projection, th);
				//G::renderAndFlatten<<< NUM_BLOCKS, THREADS >>> (area, d_projection, th); the newer DEV has this

				//stop and record
				DS_ERROR_CHECK(cudaThreadSynchronize());
				DS_ERROR_CHECK(cudaEventRecord(stop_event, 0));
				DS_ERROR_CHECK(cudaEventSynchronize(stop_event));
				DS_ERROR_CHECK(cudaEventElapsedTime(timings+i, start_event, stop_event));
			}
			fprintf(timingFile,"%.4f\n",*(min_element(timings,timings+NUM_RUNS)));

			//cleanup timers
			DS_ERROR_CHECK(cudaEventDestroy(start_event));
			DS_ERROR_CHECK(cudaEventDestroy(stop_event));
		}//-----end timing block-----//

	}//-----end angle loop-----

	DS_ERROR_CHECK(  cudaMemcpy( traceArray, d_projection, sizeof(float)*CBCT->getN(), cudaMemcpyDeviceToHost )  );
	
	// cleanup...
	//FREE SURFACE?
	DS_ERROR_CHECK( cudaFree(d_rotatedVoxData) );
	DS_ERROR_CHECK( cudaFree(d_X) );
	DS_ERROR_CHECK( cudaFree(d_Y) );
	DS_ERROR_CHECK( cudaFree(d_projection) );
	DS_ERROR_CHECK( cudaFree(d_ray) );
	DS_ERROR_CHECK( cudaFree(d_sy) );
	DS_ERROR_CHECK( cudaFree(d_sz) );
	DS_ERROR_CHECK( cudaFree(d_len) );
	DS_ERROR_CHECK( cudaUnbindTexture(G::tex_rot_vox2D) );
	DS_ERROR_CHECK( cudaFreeArray(dArray_rotatedVoxels) );
	DS_ERROR_CHECK( cudaGetLastError() );

	G::stopTimer();
	if(verbose && !timingMode) printf("DRR SUITE %s: Total %s Projection Time is %.2f ms\n","FIXEDGRID",dryRun?"Dry-Run":"Effective", G::readTimer()); 
	G::cleanupTimer();

	return false;//no error
}


bool drrSuite::getProjectionFixedGrid(
/* will only support CBCT object, not suitable for arbitrary geometry */
		  float* traceArray,	/**< [out] RESULT: array containing the result of the ray traces will have length endPtsLength (device pointer option) */
		  bool   useDevPtrs,	/**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
		  bool   useCBCTmode,
		  ds_CBCT* CBCT
	){

	DS_ERROR_CHECK( cudaSetDevice(gpuID) );
	int area;
	//if(useCBCTmode){
		area = CBCT->getArea();
	//}
	//else{ area = endPtsLength;}

	setVoxelPlanes(CBCT->getIsocenterPtr());

	namespace G = DRR_SUITE_GLOBAL;

	G::initTimer();
	G::startTimer();

	//device pointers:
	float *d_rotatedVoxData;
	float *d_X;
	float *d_Y;
	float *d_projection;
	float *d_ray;
	float *d_sy;
	float *d_sz;
	float *d_len;

	//device pointers for rotation
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_X, sizeof(float)*voxDim[0]*voxDim[1] )  );
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_Y, sizeof(float)*voxDim[0]*voxDim[1] )  );

	//device pointers for projection
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_ray, 3*sizeof(float)*area) );
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_sy,	sizeof(float)*area) );
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_sz,	sizeof(float)*area) );
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_len,   sizeof(float)*area) );

	//result array
	DS_ERROR_CHECK(  cudaMalloc( (void**)&d_projection, sizeof(float)*CBCT->getN())  );

	//temp array
	//DS_ERROR_CHECK(  cudaMalloc( (void**)&d_rotatedVoxData, sizeof(float)*voxDim[0]*voxDim[1]*voxDim[2] )  );

	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();

	/*-----3Dtexture----- (support non texture writing )
	cudaExtent rotVolumeSize = make_cudaExtent( voxDim[1], voxDim[2], voxDim[0]);//new format for voxels
	cudaArray* d_rot_volumeArray = 0;
	// create 3D array
	DS_ERROR_CHECK(  cudaMalloc3DArray( &d_rot_volumeArray, &channelDesc, rotVolumeSize )  );
	//settings to copy rotated voxels into cuda array
	cudaMemcpy3DParms rotCopyParams = {0};
	rotCopyParams.srcPtr   = make_cudaPitchedPtr( (void*)d_rotatedVoxData, rotVolumeSize.width*sizeof(float), rotVolumeSize.width, rotVolumeSize.height );
	rotCopyParams.dstArray = d_rot_volumeArray;
	rotCopyParams.extent   = rotVolumeSize;
	rotCopyParams.kind	 = cudaMemcpyDeviceToDevice;
	/-------*/
	
//SURFACE:
/*
surfaceReference* surfRefPtr = 0;
cudaGetSurfaceReference((const surfaceReference**) &surfRefPtr, "DRR_SUITE_GLOBAL::transVox_surf");
cudaArray* dArray_rotatedVoxels;
cudaMallocArray(&dArray_rotatedVoxels, &channelDesc, 8192, 128*32);//surface quilt is 16x32 patches of 512x128 (512 patches total for X layers)
cudaChannelFormatDesc channelArrayDesc;
cudaGetChannelDesc(&channelArrayDesc, dArray_rotatedVoxels);
cudaBindSurfaceToArray(*surfRefPtr, dArray_rotatedVoxels, &channelDesc);
*/
	//cudaChannelFormatDesc channelSurfDesc = cudaCreateChannelDesc(4, 0, 0, 0, cudaChannelFormatKindUnsigned);
	cudaArray* dArray_rotatedVoxels;
	//create 2D array
	//surface quilt is 16x32 patches of 512x128 (512 patches total for X layers)
///	DS_ERROR_CHECK(  cudaMallocArray(&dArray_rotatedVoxels, &channelDesc, 8192, 128*QUILT_DIM_1, cudaArraySurfaceLoadStore) );
	DS_ERROR_CHECK(  cudaMallocArray(&dArray_rotatedVoxels, &channelDesc, voxDim[1]*QUILT_DIM_0, voxDim[2]*(voxDim[0]/QUILT_DIM_0), cudaArraySurfaceLoadStore) );
	//cudaBindSurfaceToArray(transVox_surf, dArray_rotatedVoxels, &channelSurfDesc);
	/*ADD MACRO HERE*****************************/

	#if defined(__CUDA_ARCH__) & (__CUDA_ARCH__ < 200)
	#warning skipping cudaBindSurfaceToArray for compute capability < 2.0
	#else
	DS_ERROR_CHECK(  cudaBindSurfaceToArray(DRR_SUITE_GLOBAL::transVox_surf2D, dArray_rotatedVoxels)  );
	#endif

//TEXTURE SETTINGS:
	// set texture parameters
	G::tex_rot_vox2D.normalized = false;// access with normalized texture coordinates
	G::tex_rot_vox2D.filterMode = cudaFilterModeLinear;// linear interpolation
	//	.filterMode = cudaFilterModePoint;
	G::tex_rot_vox2D.addressMode[0] = cudaAddressModeClamp;// wrap texture coordinates
	G::tex_rot_vox2D.addressMode[1] = cudaAddressModeClamp;
	G::tex_rot_vox2D.addressMode[2] = cudaAddressModeWrap;

	// bind textures to arrays
	DS_ERROR_CHECK(  cudaBindTextureToArray(DRR_SUITE_GLOBAL::tex_rot_vox2D, dArray_rotatedVoxels, channelDesc )  );

	//make the x-plane max be centered in layer???
	//voxPlanesMax[0] -= voxSize[0]/2.0f;

	///FIX: case where number of columns is not evenly divisible by blockDim
	blocksPerRow = 2;//set by hand for now
	
	///TODO: investigate better data/thread locality?
	//set the blocks-per-row parameter
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_blocksPerRow, &blocksPerRow,  sizeof(int)) );

	//printf("\nAngle (deg), sin(th), cos(th), Rot (ms), texCpy (ms), Time (ms)\n----------------------\n");

	//-----Start Angle Loop-----
	for( int th=0; th<CBCT->getNumAngles(); th++){
		//printf("%3.2f,",CBCT->getAngle(th)*180.0f/acos(-1.0f));

		setCBCTgeometry(CBCT, 0.0f); //the projection angle is always "zero"
		float cosineTh = cos(CBCT->getAngle(th)+G::PI/2);
		float sineTh = sin(CBCT->getAngle(th)+G::PI/2);
		int layerVoxCount = voxDim[1]*voxDim[0]; //number of voxels in each layer layer of CT
		int rThreads = voxDim[1]; // patchWidth so long as this is not larger than maxThreads 512 (1024 fermi)
		int rBlocks = layerVoxCount/rThreads; //total number of blocks needed for a layer

		if(!dryRun){
			//GPU execution:
					
			G::preRotate<<<rBlocks,rThreads>>> (d_X,d_Y,sineTh,cosineTh);
					
			//for(int z=0; z<voxDim[2]; z++){//+z*layerVoxCount
				G::rotateLayer<<<rBlocks,rThreads>>> (d_rotatedVoxData,d_X,d_Y);//,float(z*512)+0.5f);
			//}
				
			//copy rotated voxels into cuda array and rebind texture
			//DS_ERROR_CHECK(  cudaMemcpy3D( &rotCopyParams )  );
			DS_ERROR_CHECK(  cudaThreadSynchronize()  );

			const int THREADS = CBCT->getCols()/blocksPerRow;//BLOCKS_PER_ROW;
			const int NUM_BLOCKS = CBCT->getRows()*blocksPerRow;//BLOCKS_PER_ROW;
			
			//flattening algorithm
			if(!NO_FLATTEN){
				G::renderDetector<<< NUM_BLOCKS, THREADS >>> (d_ray, d_sy, d_sz, d_len, area);
				G::flatten<<< NUM_BLOCKS, THREADS >>> (d_ray, d_sy, d_sz, d_len, area, d_projection, th);
			}
			DS_ERROR_CHECK(  cudaThreadSynchronize()  );
			DS_ERROR_CHECK( cudaGetLastError() );
		}

	}//-----end angle loop-----

	DS_ERROR_CHECK(  cudaMemcpy( traceArray, d_projection, sizeof(float)*CBCT->getN(), cudaMemcpyDeviceToHost )  );
	
	// cleanup...
	//FREE SURFACE?
	//DS_ERROR_CHECK( cudaFree(d_rotatedVoxData) );
	DS_ERROR_CHECK( cudaFree(d_X) );
	DS_ERROR_CHECK( cudaFree(d_Y) );
	DS_ERROR_CHECK( cudaFree(d_projection) );
	DS_ERROR_CHECK( cudaFree(d_ray) );
	DS_ERROR_CHECK( cudaFree(d_sy) );
	DS_ERROR_CHECK( cudaFree(d_sz) );
	DS_ERROR_CHECK( cudaFree(d_len) );
	DS_ERROR_CHECK( cudaUnbindTexture(G::tex_rot_vox2D) );
	DS_ERROR_CHECK( cudaFreeArray(dArray_rotatedVoxels) );
	DS_ERROR_CHECK( cudaGetLastError() );

	G::stopTimer();
	if(verbose && !timingMode) printf("DRR SUITE %s: Total %s Projection Time is %.2f ms\n","FIXEDGRID",dryRun?"Dry-Run":"Effective", G::readTimer()); 
	G::cleanupTimer();

	return false;//no error
}

bool drrSuite::getProjectionSiddon(
		const float* startPt,	   /**< [in] a 3-vector defining the starting point of ray trace in volume coord. sys. */
		const float* endPtsX,	   /**< [in] an array defining the X element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
		const float* endPtsY,	   /**< [in] an array defining the Y element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
		const float* endPtsZ,	   /**< [in] an array defining the Z element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
		const int	endPtsLength,  /**< [in] the length of the X Y and Z end point vectors */
		const float* volOffset,	 /**< [in] a 3-vector defining the origin of the volume coord. sys. relative to (0,0,0) corner (isocenter) */
			  float* traceArray,	/**< [out] RESULT: array containing the result of the ray traces will have length endPtsLength (device pointer option) */
			  bool   useDevPtrs,	/**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
			  bool   useCBCTmode,
			  ds_CBCT* CBCT
	){

	if(useCBCTmode){
		if( CBCT->getCols()%DS_GPU_THREADS ){ //if there is a remainder
			printf("DRR SUITE %s ERROR: In CBCT mode, the number of columns (%i) must be an integer multiple of %i.\n",traceType==TRILINEAR?"TRILINEAR":"SIDDON",CBCT->getCols(),DS_GPU_THREADS);
			return true;
		}
	}

	///TODO: fix problem with setting device
	DS_ERROR_CHECK( cudaSetDevice(gpuID) );

	int projArea;
	if(useCBCTmode){ projArea = CBCT->getArea();}
	else{ projArea = endPtsLength;}

	///TODO: make block/thread count more robust
	//block and grid variables
	int blkGeo, grdGeo;
	blkGeo = DS_GPU_THREADS;
	grdGeo = projArea/blkGeo + 1;

	namespace G = DRR_SUITE_GLOBAL;
	G::initTimer();
	G::startTimer();

	//device pointers:
	float *d_projection; //projection array
	float *d_pts_x;	  //an array of the x componants of pixels (room coords.)
	float *d_pts_y;	  //an array of the y componants of pixels (room coords.)
	float *d_pts_z;	  //an array of the z componants of pixels (room coords.)

	///TODO: investigate use of pitched pointers
	if(useDevPtrs && useCBCTmode){
		//simply copy pointer
		d_projection = traceArray;
		//set unused pointers to null
		d_pts_x = NULL;
		d_pts_y = NULL;
		d_pts_z = NULL;

	}else if(useDevPtrs && !useCBCTmode){
		//simply copy pointers when passed device pointers
		d_projection = traceArray;
		d_pts_x = (float*)endPtsX;
		d_pts_y = (float*)endPtsY;
		d_pts_z = (float*)endPtsZ;

	}else if(!useDevPtrs && useCBCTmode){
		//allocate device pointer for projection data:
		if(economy){
			//we only render one projection on the GPU at a time when not using dev pointers
			DS_ERROR_CHECK( cudaMalloc((void**)&d_projection,  CBCT->getArea()*sizeof(float)) );
		}else{
			DS_ERROR_CHECK( cudaMalloc((void**)&d_projection,  CBCT->getN()*sizeof(float)) );
		}
		//set unused pointers to null
		d_pts_x = NULL;
		d_pts_y = NULL;
		d_pts_z = NULL;

	}else if(!useDevPtrs && !useCBCTmode){
		//allocate device pointers for projection and points data when not passed device pointers:
		DS_ERROR_CHECK( cudaMalloc((void**)&d_projection,  projArea*sizeof(float)) );
		DS_ERROR_CHECK( cudaMalloc((void**)&d_pts_x,	   projArea*sizeof(float)) );
		DS_ERROR_CHECK( cudaMalloc((void**)&d_pts_y,	   projArea*sizeof(float)) );
		DS_ERROR_CHECK( cudaMalloc((void**)&d_pts_z,	   projArea*sizeof(float)) );
		//copy pixel points to device
		DS_ERROR_CHECK( cudaMemcpy( d_pts_x, endPtsX, projArea*sizeof(float), cudaMemcpyHostToDevice ) );
		DS_ERROR_CHECK( cudaMemcpy( d_pts_y, endPtsY, projArea*sizeof(float), cudaMemcpyHostToDevice ) );
		DS_ERROR_CHECK( cudaMemcpy( d_pts_z, endPtsZ, projArea*sizeof(float), cudaMemcpyHostToDevice ) );
	}

	// set CBCT mode bool on device
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_CBCTmode,   &useCBCTmode, sizeof(bool) ) );

	// set the max element constant on the device
	DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_maxElement, &projArea,   sizeof(int)) );

	// set sampling frequency if fixed stride
	if(traceType==TRILINEAR){
		// set the max element constant on the device
		DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_samplingFreq, &samplingFreq,   sizeof(int)) );
	}

	if(useCBCTmode){ //need to loop through CBCT modes
		//this function might be useful later (when simulating helical CBCT)
		setVoxelPlanes(CBCT->getIsocenterPtr());
		DS_ERROR_CHECK( cudaGetLastError() );

		///FIX: case where number of columns is not evenly divisible by blockDim
		int blocksPerRow = CBCT->getCols()/blkGeo;
		
		///TODO: investigate better data/thread locality?
		//set the blocks-per-row parameter
		DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_blocksPerRow, &blocksPerRow,  sizeof(int)) );

		FILE* timingFile;

		if(timingMode){
			//open timing file for output
			timingFile = fopen(timingFileName,"w");
			fprintf(timingFile,"[%s ALGORITHM | BEST OF %i KERNEL RUN TIMES]\nAngle (deg), Projection Time (ms)\n---------------------------------\n",traceType==TRILINEAR?"TRILINEAR":"SIDDON'S",NUM_RUNS);
		}

		for( int th=0; th < CBCT->getNumAngles(); th++){

			// set CBCT geometry member variables and GPU constants:
			setCBCTgeometry(CBCT,th);

			if(!economy){ //(with or without device pointers) save all projections to device pointer
				if(!dryRun){
					if(traceType == SIDDON){
						G::siddonDRR<<< grdGeo,blkGeo >>>(NULL,NULL,NULL,d_projection+th*CBCT->getArea());
					}else{//TRILINEAR
						G::trilinearDRR<<< grdGeo,blkGeo >>>(NULL,NULL,NULL,d_projection+th*CBCT->getArea());
					}
					DS_ERROR_CHECK( cudaThreadSynchronize() );
					DS_ERROR_CHECK( cudaGetLastError() );
				}
			}else if(!useDevPtrs){ //(WE ARE IN ECONOMY MODE) each projection will be copied to host after rendering
				if(!dryRun){
					if(traceType == SIDDON){
						G::siddonDRR<<< grdGeo,blkGeo >>>(NULL,NULL,NULL,d_projection);
					}else{//TRILINEAR
						G::trilinearDRR<<< grdGeo,blkGeo >>>(NULL,NULL,NULL,d_projection);
					}
					DS_ERROR_CHECK( cudaThreadSynchronize() );
					DS_ERROR_CHECK( cudaGetLastError() );
				}
				//copy single projection area back to host
				DS_ERROR_CHECK( cudaMemcpy(traceArray+th*CBCT->getArea(),d_projection,CBCT->getArea()*sizeof(float),cudaMemcpyDeviceToHost) );
			}else{ //device pointers and economy mode are not compatable: what are you trying to do?
				printf("DRR SUITE %s ERROR: In CBCT mode, using device pointers is not compatable with economy mode.\n",traceType==TRILINEAR?"TRILINEAR":"SIDDON");
				return true;
			}

			//-----start timing mode-----//
			if(!dryRun && timingMode){

				fprintf(timingFile,"%3.2f,",CBCT->getAngle(th)*180.0f/G::PI);

				//create a vector to store run times
				float* timings = new float[NUM_RUNS];

				//initialize cuda event timers
				cudaEvent_t start_event, stop_event;
				int eventflags = cudaEventBlockingSync;
				DS_ERROR_CHECK( cudaEventCreateWithFlags(&start_event, eventflags) );
				DS_ERROR_CHECK( cudaEventCreateWithFlags(&stop_event,  eventflags) );

				if(traceType == SIDDON){// time siddon kernel
					//#pragma unroll
					for(int i=0; i<NUM_RUNS; i++){
						//start a cuda timer
						DS_ERROR_CHECK(cudaEventRecord(start_event, 0));

						G::siddonDRR<<< grdGeo,blkGeo >>>(NULL,NULL,NULL,d_projection);

						//stop and record
						DS_ERROR_CHECK(cudaThreadSynchronize());
						DS_ERROR_CHECK(cudaEventRecord(stop_event, 0));
						DS_ERROR_CHECK(cudaEventSynchronize(stop_event));
						DS_ERROR_CHECK(cudaEventElapsedTime(timings+i, start_event, stop_event));
						DS_ERROR_CHECK(cudaThreadSynchronize());
						if(th==0){
							fprintf(stdout,"angle zero,%i:%f\n",i,timings[i]);
						}
					}
					fprintf(timingFile,"%.4f\n",*(min_element(timings,timings+NUM_RUNS)));

				}else{//time trilinear
					//#pragma unroll
					for(int i=0; i<NUM_RUNS; i++){
						//start a cuda timer
						DS_ERROR_CHECK(cudaEventRecord(start_event, 0));

						G::trilinearDRR<<< grdGeo,blkGeo >>>(NULL,NULL,NULL,d_projection);

						//stop and record
						DS_ERROR_CHECK(cudaThreadSynchronize());
						DS_ERROR_CHECK(cudaEventRecord(stop_event, 0));
						DS_ERROR_CHECK(cudaEventSynchronize(stop_event));
						DS_ERROR_CHECK(cudaEventElapsedTime(timings+i, start_event, stop_event));
						DS_ERROR_CHECK(cudaThreadSynchronize());
					}
					fprintf(timingFile,"%.4f\n",*(min_element(timings,timings+NUM_RUNS)));
				}
				//cleanup timers
				DS_ERROR_CHECK(cudaEventDestroy(start_event));
				DS_ERROR_CHECK(cudaEventDestroy(stop_event));
			}//-----end timing block-----//

		}

		if(!useDevPtrs && !economy){
			//copy all projection data back
			DS_ERROR_CHECK( cudaMemcpy(traceArray,d_projection, CBCT->getN()*sizeof(float), cudaMemcpyDeviceToHost) );
		}

		if(!useDevPtrs){ // (regardless of economy mode)
			//cleanup GPU memory
			DS_ERROR_CHECK( cudaFree(d_projection) );
		}
	
		if(timingMode){
			fclose(timingFile);
		}
		
	}else{ //process one general projection only

		if(economy){
			printf("DRR SUITE %s WARNING: Economy mode does nothing when rendering a general projection.\n",traceType==TRILINEAR?"TRILINEAR":"SIDDON");
		}

		if(timingMode){
			printf("DRR SUITE %s WARNING: Timing not avalable when rendering a general projection.\n",traceType==TRILINEAR?"TRILINEAR":"SIDDON");
		}

		//this function might be useful later (when simulating helical CBCT)
		setVoxelPlanes(volOffset);

		DS_ERROR_CHECK( cudaMemcpyToSymbol(DRR_SUITE_GLOBAL::dc_source,	 startPt,	  3*sizeof(float)) );
		if(!dryRun){
			if(traceType == SIDDON){
				G::siddonDRR<<< grdGeo,blkGeo >>>(d_pts_x, d_pts_y, d_pts_z,d_projection);
			}else{ //TRILINEAR
				G::trilinearDRR<<< grdGeo,blkGeo >>>(d_pts_x, d_pts_y, d_pts_z,d_projection);
			}
			DS_ERROR_CHECK( cudaThreadSynchronize() );
			DS_ERROR_CHECK( cudaGetLastError() );
		}
		if(!useDevPtrs){
			//copy projection back to host
			DS_ERROR_CHECK( cudaMemcpy(traceArray,d_projection, projArea*sizeof(float), cudaMemcpyDeviceToHost) );

			//cleanup GPU memory
			DS_ERROR_CHECK( cudaFree(d_projection) );
			DS_ERROR_CHECK( cudaFree(d_pts_x)	  );
			DS_ERROR_CHECK( cudaFree(d_pts_y)	  );
			DS_ERROR_CHECK( cudaFree(d_pts_z)	  );
		}
	}
	
	G::stopTimer();
	if(verbose /*&& !timingMode*/) printf("DRR SUITE %s: Total %s Projection Time is %.2f ms\n",traceType==TRILINEAR?"TRILINEAR":"SIDDON",dryRun?"Dry-Run":"Effective", G::readTimer()); 
	G::cleanupTimer();

	return false;

};
bool drrSuite::getProjection(
		const float* startPt,	   /**< [in] a 3-vector defining the starting point of ray trace in volume coord. sys. */
		const float* endPtsX,	   /**< [in] an array defining the X element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
		const float* endPtsY,	   /**< [in] an array defining the Y element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
		const float* endPtsZ,	   /**< [in] an array defining the Z element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
		const int	endPtsLength,  /**< [in] the length of the X Y and Z end point vectors */
		const float* volOffset,	 /**< [in] a 3-vector defining the origin of the volume coord. sys. relative to (0,0,0) corner (isocenter) */
			  float* traceArray,	/**< [out] RESULT: array containing the result of the ray traces will have length endPtsLength (device pointer option) */
			  bool   useDevPtrs	 /**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
	){
	
	bool useCBCTmode = false;

	if(!voxelsSet){
		printf("DRR SUITE ERROR: plese setVoxels before calling getProjection\n");

	}else{
		switch (traceType){
			case  SIDDON:
				return getProjectionSiddon(startPt, endPtsX, endPtsY, endPtsZ, endPtsLength,
										   volOffset, traceArray, useDevPtrs, useCBCTmode, NULL);
			case  TRILINEAR:
				return getProjectionSiddon(startPt, endPtsX, endPtsY, endPtsZ, endPtsLength,
										   volOffset, traceArray, useDevPtrs, useCBCTmode, NULL);
			default:
				printf("DRR SUITE ERROR: specified algorithm type [%i] is not supported.\n",traceType);
		}// END SWITCH
	}
	return true;
}
bool drrSuite::getProjection(
			  ds_CBCT* CBCTptr,
			  float* traceArray,	/**< [out] RESULT: array containing the result of the ray traces will have length endPtsLength (device pointer option) */
			  bool   useDevPtrs	 /**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
	){
	
	bool useCBCTmode = true;
	
	if(!voxelsSet){
		printf("DRR SUITE ERROR: plese setVoxels before calling getProjection\n");

	}else{
		switch (traceType){
			case SIDDON:
				//ds_CBCT* CBCTptr = NULL;
				return getProjectionSiddon(NULL, NULL, NULL, NULL, 0,
				                           NULL, traceArray, useDevPtrs, useCBCTmode, CBCTptr);
			case TRILINEAR:
				return getProjectionSiddon(NULL, NULL, NULL, NULL, 0,
				                           NULL, traceArray, useDevPtrs, useCBCTmode, CBCTptr);
			case FIXEDGRID_OLD:
				return getProjectionFixedGrid(traceArray, useDevPtrs, useCBCTmode, CBCTptr);
			case FIXEDGRID_NEW:
				return getProjectionFixedGrid_DEV(traceArray, useDevPtrs, useCBCTmode, CBCTptr);
			case FIXEDGRID_REUSE:
				ROTATION_REUSE_MODE=true;
				printf("DRR SUITE ALERT: Using Rotation Reuse Mode (EXPERIMENTAL).\n");
				return getProjectionFixedGrid_DEV(traceArray, useDevPtrs, useCBCTmode, CBCTptr);
			default:
				printf("DRR SUITE ERROR: specified algorithm type [%i] is not supported.\n",traceType);
		}// END SWITCH
	}
	return true;
}

//support arbitrary detector geometry:
bool drrSuite::getArbProjection(
		ds_CBCT* CBCTptr,   /**< [in] the configuration for the CBCT setup */
		float* traceArray,	/**< [out] RESULT */
		bool   useDevPtrs /**< [in] optional parameter (default = false) can be set to true to specify endPts
										and traceArray as allocated and set on the device. */
	){
		bool useCBCTmode = true;
		bool useArbCBCTmode = true;
	
	if(!voxelsSet){
		printf("DRR SUITE ERROR: plese setVoxels before calling getProjection\n");
	}else{
		switch (traceType){
			case SIDDON:
				//ds_CBCT* CBCTptr = NULL;
//				return getProjectionSiddon(NULL, NULL, NULL, NULL, 0,
//										   NULL, traceArray, useDevPtrs, useCBCTmode, CBCTptr);
				printf("DRR SUITE ERROR: Selected algorithm not yet supported in Arbitrary CBCT mode\n");
				return true; //error
			case TRILINEAR:
//				return getProjectionSiddon(NULL, NULL, NULL, NULL, 0,
//										   NULL, traceArray, useDevPtrs, useCBCTmode, CBCTptr);
				printf("DRR SUITE ERROR: Selected algorithm not yet supported in Arbitrary CBCT mode\n");
				return true; //error
			case FIXEDGRID_OLD:
//				return getProjectionFixedGrid(traceArray, useDevPtrs, useCBCTmode, CBCTptr);
				printf("DRR SUITE ERROR: Selected algorithm not yet supported in Arbitrary CBCT mode\n");
				return true; //error
			case FIXEDGRID_NEW:
//				return getProjectionFixedGrid_DEV(traceArray, useDevPtrs, useCBCTmode, useArbCBCTmode, CBCTptr);
				printf("DRR SUITE ERROR: Selected algorithm not yet supported in Arbitrary CBCT mode\n");
			case FIXEDGRID_REUSE:
//				ROTATION_REUSE_MODE=true;
//				printf("DRR SUITE ALERT: Using Rotation Reuse Mode (EXPERIMENTAL).\n");
//				return getProjectionFixedGrid_DEV(traceArray, useDevPtrs, useCBCTmode, CBCTptr);
			default:
				printf("DRR SUITE ERROR: specified algorithm type [%i] is not supported.\n",traceType);
		}// END SWITCH
	}
	return true; //error
}

#endif
