//Author: Michael Folkerts, mmfolkerts@gmail.com, http://bit.ly/folkerts
//Date: March 2012

#include "drrSuite.cuh" //Mike's new library
#include <string.h>
#include <stdio.h>
#include <argtable2.h>

//only shortcuting what is used
#include <iostream>
using std::cout;
using std::endl;

#include <fstream>
using std::ofstream;
using std::ifstream;
using std::ios;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::fill;

/** create an argv and argc so argtable2 can parse arguments from a file **/
bool parseExtraInput(const char* fileName,int argc, char** argv, int* ArgC, char*** ArgV){
// supports # comments
	cout << "Reading Configuration from: "<< fileName <<endl;
	size_t charCount,filePos;
	ifstream inFile(fileName,ifstream::in);
	string testString;
	vector<string> argList;

	if( inFile.fail() ){
		cout << "WARNING: Could not read command arguments file: " << fileName << endl;
		return 1; //error
	}else{
		while (! inFile.eof() ){
			getline(inFile,testString,'\n');
			//cout << "before:" << testString << "(" << testString.length() <<")" <<endl;
			//strip # comments
			filePos = testString.find_first_of("#");
			if( filePos != string::npos ){
				testString.erase(filePos,testString.npos); //erases till end of line
			}
			//clear white space (to assure compatability with argtable2)
			filePos = testString.find_first_of(" \t");
			while ( filePos != string::npos ){
				//cout <<"-"<<filePos;
				testString.erase(filePos,1);
				filePos = testString.find_first_of(" \t");
			};
			//cout << "after:" << testString << "(" << testString.length() <<")" <<endl;
			//add to arg list if not empty
			if( testString.length() > 0 ){
				argList.push_back(testString);
			}
		}
		inFile.close();
		//fflush(stdout);
		//cout << "Number of xargs:" << argList.size() << endl;
		*ArgC = argList.size()+argc;
		*ArgV = new char*[argList.size()+argc];
		char fakeAppName[] = "foolingArgtable2";
		(*ArgV)[0] = fakeAppName;
		//make new arg list
		for( int i = 0; i<argList.size(); i++ ){
			(*ArgV)[i+1] = new char[argList[i].length()+1];
			//copy from vector into char**
			strcpy((*ArgV)[i+1],argList[i].c_str());
			cout << " added: " << (*ArgV)[i+1] << endl;
		}
		//append actual command line args
		for( int i = 0; i<argc-1; i++ ){
			(*ArgV)[argList.size()+i+1] = argv[i+1];
			cout << " kept: " << (*ArgV)[argList.size()+i+1] << endl;
		}
		return true;
	}
}

/*----------------------------------------------------------------------
 Portable function to set a socket into nonblocking mode.
 Calling this on a socket causes all future read() and write() calls on
 that socket to do only as much as they can immediately, and return 
 without waiting.
 If no data can be read or written, they return -1 and set errno
 to EAGAIN (or EWOULDBLOCK).
 Thanks to Bjorn Reese for this code.
----------------------------------------------------------------------
int setNonblocking(int fd)
{
    int flags;

    // If they have O_NONBLOCK, use the Posix way to do it 
#if defined(O_NONBLOCK)
    // Fixme: O_NONBLOCK is defined but broken on SunOS 4.1.x and AIX 3.2.5. 
    if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
        flags = 0;
    return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
    // Otherwise, use the old way of doing it 
    flags = 1;
    return ioctl(fd, FIOBIO, &flags);
#endif
}  
*/

/** a simple file saving template */
template <class T>
void saveDataFile(const char* fileName, T* array, size_t numElements){
	// save the data
	ofstream outFile(fileName,ios::binary);
	outFile.sync_with_stdio(false);
	outFile.write((char*)array,sizeof(T)*numElements);
	outFile.close();
	cout << "Wrote " << sizeof(T)*numElements << " bytes to file: " << fileName << endl;
}

/** a simple file saving template */
template <class T>
void saveDataFileC(const char* fileName, T* array, size_t numElements){
	// save the data
	FILE* outFile = fopen(fileName,"wb");
	//setNonblocking(outFile);
	//ofstream outFile(fileName,ios::binary);
	size_t writtenElements = fwrite(array,sizeof(T),numElements,outFile);
	//outFile.write((char*)array,*);
	//fclose(outFile);
	cout << "Wrote " << sizeof(T)*writtenElements << " bytes to file: " << fileName << endl;
}

/** a simple file reading template */
template <class T>
bool readDataFile(const char* fileName, T* array, size_t numElements){
	// save the data
	ifstream inFile(fileName,ios::binary);
	if(inFile.fail()){
		cout <<"WARNING: Failed to open file: " << fileName << endl;
		return 1; //error
	}else{
		// get length of file:
		inFile.seekg (0, ios::end);
		size_t length = inFile.tellg();
		inFile.seekg (0, ios::beg);

		if(length/sizeof(T) == numElements){
			inFile.read((char*)array,sizeof(T)*numElements);
			inFile.close();
			cout << "Read " << sizeof(T)*numElements << " bytes from file: " << fileName << endl;
			return 0; //no error
		}else{
			cout << "WARNING: Size of input file does not match given parameters! (" << fileName << " has " << length/sizeof(T) << " elements)" << endl;
			return 1; //error
		}
	}
}

//argtable constants
//struct arg_int *x,*y,*z;
struct arg_lit  *HELP,*argDryRun,*argReuse;
struct arg_end  *end;
struct arg_file *argOutFile, *argInFile, *argXargs,*argTiming;
struct arg_int  *argDimensionVol, *argImageDim,
                *argAlgorithm,*argAngleCount,*argGPU;
struct arg_dbl  *argResolutionVox, *argIsocenter,
                *argImageSize, *argImageShift,
                *argAngleStart, *argAngleStep,
                *argImageSAD, *argImageSID;

void testDRR(int ALGORITHM){
    cout << endl;

	// create new drrSuite object
	int gpuID;
	if(argGPU->count == 1){
		gpuID = argGPU->ival[0];
	}else{
		gpuID = 0;
	}
	
	drrSuite* traceTest = new drrSuite(gpuID,drrSuite_algorithm_t(ALGORITHM)); //GPU ID, algorithm
	bool errorCheck;
	
	// simple GPU API test
	cout << "Running drrSuite on GPU #" << traceTest->getGPU() << endl;

	if(argReuse->count == 1){
		cout << "Setting reuse mode to true..." << endl;
		traceTest->ROTATION_REUSE_MODE = true; //turns on rotation reuse!
	}

	// default voxel volume information
	int    volDim[]  = {512,512,128};
	float  voxSize[] = {2.0f,2.0f,2.5f}; //mm

	if(argDimensionVol->count == 3){
		volDim[0] = argDimensionVol->ival[0];
		volDim[1] = argDimensionVol->ival[1];
		volDim[2] = argDimensionVol->ival[2];
		cout << "Phantom dimension set: " << volDim[0] << " " << volDim[1] << " " << volDim[2] << endl;
	}else{
		// fill the volume with uniform density
		cout << "Using default dimensions:  " << volDim[0] << " " << volDim[1] << " " << volDim[2] << endl;
	}

	//fill with default attenuation first
	float* volArray  = new float[ volDim[0]*volDim[1]*volDim[2] ];
	fill(volArray, volArray+volDim[0]*volDim[1]*volDim[2], 1.0f);;

	if(argInFile->count == 1){
		if( readDataFile(argInFile->filename[0],volArray, volDim[0]*volDim[1]*volDim[2]) ){ //read fails
				cout << "Using default uniform cube as phantom..." << endl;
		}
	}

	if(argResolutionVox->count == 3){
		voxSize[0] = float(argResolutionVox->dval[0]);
		voxSize[1] = float(argResolutionVox->dval[1]);
		voxSize[2] = float(argResolutionVox->dval[2]);
		cout << "Voxel sizes set: " << voxSize[0] << " " << voxSize[1] << " " << voxSize[2] << endl;
	}else{
		cout << "Using default voxel sizes:" << voxSize[0] << " " << voxSize[1] << " " << voxSize[2] << endl;
	}

	// set voxel information on GPU
	traceTest->setVoxels(volDim, voxSize, volArray);

	// set default isocenter
	float isocenter[] = {float(volDim[0])*voxSize[0]/2.0,float(volDim[1])*voxSize[1]/2.0,float(volDim[2])*voxSize[2]/2.0}; // mm (vector from corner of data to origin)

	if(argIsocenter->count == 3){
		isocenter[0] = float(argIsocenter->dval[0]);
		isocenter[1] = float(argIsocenter->dval[1]);
		isocenter[2] = float(argIsocenter->dval[2]);
		cout << "Isocenter set: " << isocenter[0] << " " << isocenter[1] << " " << isocenter[2] << endl;
	}else{
		cout << "Using default isocenter: " << isocenter[0] << " " << isocenter[1] << " " << isocenter[2] << endl;
	}

	//test CBCT mode
	cout << "\n** Testing CBCT DRR mode ** [algNo. " << ALGORITHM << "]" << endl;

	float* traceArray; // RESULT: array for the result of the ray traces

	// use the DRR Suite (ds) CBCT class
	ds_CBCT* testCBCT = new ds_CBCT();
		// DEFAULTS: SID = 1500 mm, SAD = 1000 mm
		// 640x480 pixel detector of size 400x300 mm
		// isocenter = (0,0,0)
		// 36 projections, one every 10 degrees starting from 0 deg

	//set CBCT configuration
	if(argImageDim->count == 2){
		//columns, rows, width (mm), height (mm)
		testCBCT->setDetectorRes(argImageDim->ival[0],argImageDim->ival[1]);
	}
	
	//set CBCT configuration
	if(argImageSize->count == 2){
		//columns, rows, width (mm), height (mm)
		testCBCT->setDetectorSize(float(argImageSize->dval[0]),float(argImageSize->dval[1]));
	}

	if(argIsocenter->count == 3){
		testCBCT->setIsocenter(float(argIsocenter->dval[0]),float(argIsocenter->dval[1]),float(argIsocenter->dval[2])); // mm
	}else{
		testCBCT->setIsocenter(isocenter[0],isocenter[1],isocenter[2]); // mm
	}

	if(argAngleCount->count == 1 && argAngleStart->count == 1 && argAngleStep->count == 1){
		testCBCT->setAnglesDeg(argAngleCount->ival[0], float(argAngleStart->dval[0]), float(argAngleStep->dval[0]));
	}else{
		testCBCT->setAnglesDeg(100,0.0f,1.8f); // number of angles, start angle, angle step size
	}

	if(argImageShift->count == 2){
		testCBCT->setShifts(float(argImageShift->dval[0]),float(argImageShift->dval[1]));
	}else{
		testCBCT->setShifts(0.0f,0.0f); // detector shifts (right,up) from beams-eye-view (mm)
	}

	if(argImageSID->count == 1){
		testCBCT->setSID(float(argImageSID->dval[0]));
	}

	if(argImageSAD->count == 1){
		testCBCT->setSAD(float(argImageSAD->dval[0]));
	}

	// set traceArray to be large enough to fit all the projections
	traceArray = new float[testCBCT->getN()]; 
	fill(traceArray, traceArray+testCBCT->getN(), 0.0);

	// set timing mode
	if(argTiming->count == 1){
		traceTest->setTimingMode(argTiming->filename[0]);
	}

	// include dry run upon request
	if(argDryRun->count == 1){
		traceTest->dryRun = true;
		errorCheck = traceTest->getProjection(testCBCT, traceArray);
		traceTest->dryRun = false;
	}
	// generate projections in cbct mode by passing the ds_CBCT object to the drrSuite member function
	errorCheck = traceTest->getProjection(testCBCT, traceArray);

	// save the data
	if( argOutFile->count == 1){
		saveDataFile(argOutFile->filename[0], traceArray, testCBCT->getN());
	}else{
		saveDataFile("cbct_projections.floats", traceArray, testCBCT->getN());
	}
	
	cout << "\nProjection is " << testCBCT->getCols() << " cols by " << testCBCT->getRows()
	     << " rows by " << testCBCT->getNumAngles() << " layers, stored as "
	     << sizeof(float) << "-byte floats in row-major order." << endl;

	//cleanup
	cout << endl;
	delete testCBCT;
	//traceTest->resetGPU();
	delete traceTest;
	delete volArray;
	delete traceArray;
}


int main(int argc, char **argv){

	cout<<"\n[DRR EXPERIMENT] Running...\n\n";

	void *argtable[]={
		//extra arguments from file
		argXargs = arg_file0(NULL,NULL,"<file>","(optional) config file"),

		HELP = arg_lit0(NULL, "help", "Show this help menu"),
		argAlgorithm = arg_int0("a","Algorithm","<int>","Siddon=0 (default), Trilinear=1, FixedGridNEW=3"),
		argReuse = arg_lit0("r", "reuse-mode", "Set to re-use voxel rotations in FixedGridNew"),
		argDryRun = arg_lit0("d", "Dry-run", "Includes overhead run with no GPU kernels"),
		argTiming = arg_file0("t", "TimingFile","<file>", "Saves best time of 10 runs for each kernel to file"),
		argGPU = arg_int0("g","gpuID","<int>","Specify GPU to use (default 0)"),
		//phantom information
		argInFile = arg_file0("i",NULL,"<file>", "input file (default is uniform cube)"),
		argDimensionVol = arg_intn(NULL, "VolDimension","<int>",0,3,"Number of data elements (repeat for x,y,z)."),
		argResolutionVox = arg_dbln(NULL, "VoxSize","<float>",0,3,"Size of pixels/voxels in mm (repeat for x,y,z)."),
		argIsocenter = arg_dbln(NULL, "Isocenter","<float>",0,3,"(optional) isocenter (mm)."),

		//projection information
		argOutFile = arg_file0("o",NULL,"<file>", "output file"),
		argImageDim = arg_intn(NULL, "ImageResolution","<int>",0,2,"Number of columns (repeat for rows)."),
		argImageSize = arg_dbln(NULL, "ImageSize","<float>",0,2,"Width of imager plane in mm (repeat Height)."),
		argImageShift = arg_dbln(NULL, "ImageShift","<float>",0,2,"Shift detector right from beams-eye-view in mm     (repeat for shift up)."),
		argImageSID = arg_dbl0(NULL, "SID","<float>","Source to Imager Distance (mm)."),
		argImageSAD = arg_dbl0(NULL, "SAD","<float>","Source to rotation Axis (isocenter) Distance (mm)."),

		//CBCT information
		argAngleStart = arg_dbl0(NULL, "AngleStart","<float>","Starting gantry angle (deg)."),
		argAngleStep = arg_dbl0(NULL, "AngleStep","<float>","Gantry angle increment (deg)."),
		argAngleCount = arg_int0(NULL, "AngleCount","<int>","Number total of projections."),

		end = arg_end(20)
	};

	if(arg_nullcheck(argtable) != 0)
		printf("error: insufficient memory\n");

	int nerrors = arg_parse(argc,argv,argtable);
	int algorithm=0;
	
	//help message
	if( HELP->count ){
		printf("\nUsage: %s",argv[0]);
		arg_print_syntax(stdout,argtable,"\n\n");
		arg_print_glossary_gnu(stdout,argtable);
		arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
		return 0;
	}

	if (argXargs->count == 1){
		int xArgsC;
		char** xArgsV;
		parseExtraInput(argXargs->filename[0],argc,argv,&xArgsC,&xArgsV);
		nerrors += arg_parse(xArgsC,xArgsV,argtable);
		delete xArgsV;
	}

	if (nerrors==0)
	{
		if(argAlgorithm->count == 1)
		{
			algorithm = argAlgorithm->ival[0];
		}

		//run the program with specified algorithm
		testDRR(algorithm); // 0 = SIDDON, 1 = TRILINEAR, 2 = FIXEDGRID

	}else{
		arg_print_errors(stdout,end,argv[0]);
	}

	arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));

	return 0;

}
