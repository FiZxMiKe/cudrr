#ifndef DRR_SUITE_EXTERN_C
#define DRR_SUITE_EXTERN_C
//external C wrapping for python interface

extern "C" {
    drrSuite* ds_new(int gpu, int algorithm){ return new drrSuite(gpu,drrSuite_algorithm_t(algorithm)); }
    
    void ds_delete(drrSuite* ds){ delete ds; }
	
	bool ds_setVoxels(drrSuite* ds, int* dim, float* size, float* array){
		return ds->setVoxels(dim,size,array);
	}
	
	bool ds_getProjection(drrSuite* ds, ds_CBCT* config, float* result){
		return ds->getProjection(config,result);
	}

    int ds_getGPU(drrSuite* ds){
        return ds->getGPU();
    }

    ds_CBCT* cb_new(){
    	return new ds_CBCT();
    }

    void cb_delete(ds_CBCT* cb){
    	delete cb;
    }

    size_t cb_getN(ds_CBCT* cb){
        return cb->getN();
    }

    size_t cb_getArea(ds_CBCT* cb){
        return cb->getArea();
    }

    void cb_setIsocenter(ds_CBCT* cb, const float x, const float y, const float z){
        cb->setIsocenter(x,y,z);
    }

    float cb_getIsocenterX(ds_CBCT* cb){return cb->getIsocenterX();}
    float cb_getIsocenterY(ds_CBCT* cb){return cb->getIsocenterY();}
    float cb_getIsocenterZ(ds_CBCT* cb){return cb->getIsocenterZ();}

    void cb_setAnglesDeg(ds_CBCT* cb, int  numAng, float startAng, float deltaAng){
        cb->setAnglesDeg(numAng,startAng,deltaAng);
    }

    float cb_getAngle(ds_CBCT* cb, int i){
        return cb->getAngle(i);
    }

    int cb_getNumAngles(ds_CBCT* cb){
        return cb->getNumAngles();
    }

    void cb_setShifts(ds_CBCT* cb,float rt_px, float up_px){
    /** set detector shifts from beams eye view (right pixels,up pixels) */
        cb->setShifts(rt_px,up_px);
    }

    int echoInt(int i){ return i; }//tester
}
#endif