//Author: Michael Folkerts, mmfolkerts@gmail.com, http://bit.ly/folkerts
//Date: March 2012

#ifndef DRR_SUITE_H
#define DRR_SUITE_H
#include <math.h>
#include "cuda_runtime.h" // links to shared lib

/** this type is used to define the algorithm implemented in the function */
enum drrSuite_algorithm_t {SIDDON, TRILINEAR, FIXEDGRID_OLD, FIXEDGRID_NEW, FIXEDGRID_REUSE};

///todo: implement this, may be much cleaner:
//enum drrSuite_pointers_t {HOST, DEVICE};

struct ds_Detector{
	int cols;     // number of pixel column in detector
	int rows;     // number of pixel rows in detector
	float width;  // physical width of detector (mm)
	float height; // physical height of detector
	float shiftRt; // (+) shifts detector right from beams-eye-view (pixels)
	float shiftUp;  // (+) shifts detector (pixels)
};

// this is the CBCT object with recommended defaults. all units should be in mm
class ds_CBCT{
  public:

	//use these to store pre-computed detector data when using arbCBCT mode in drrSuite
	//you still must set the ANGLES for reuse to work
	///TODO implement safety checks
	/** a list of the pointers to 3-vector defining detector geometry **/
	float* source;
	float* corner;
	float* pixelRight;
	float* pixelDown; 

	ds_CBCT(){
		SID = 1500;       // source to imager distance (mm)
		SAD = 1000;       // source to axis distance (mm)

		detector.cols   = 640;   // number of pixel column in detector
		detector.rows   = 480;   // number of pixel rows in detector
		detector.width  = 400.0f; // physical width of detector (mm)
		detector.height = 300.0f; // physical height of detector
		detector.shiftRt = 0.0f; // detector shift in units of pixels
		detector.shiftUp = 0.0f; // detector shift in units of pixels

		isocenter = new float[3];
		setIsocenter(0.0f,0.0f,0.0f);
		angles = new float[1]; //so I dont try to delete the null pointer in the next function call
		setAnglesDeg(36, 0.0f, 10.0f); //number, start angle (deg), delta angle (deg)

	};

	~ds_CBCT(){
		delete angles;
		delete isocenter;
	};

	//set functions

	/** set isocenter based on user's pointer 
	setIsocenter(float* userPointer){
		for(int i=0; i<3; i++){
			isocenter[i] = userPointer[i];
		}
	}
	*/

	/** set isocenter based on 3 floats */
	void setIsocenter(const float x, const float y, const float z){
		isocenter[0] = x;
		isocenter[1] = y;
		isocenter[2] = z;
	}

	/** set Source to Imager Distance */
	void setSID(float SID_in){ SID = SID_in; };
	/** set Source to rotation Axis (isocenter) Distance */
	void setSAD(float SAD_in){ SAD = SAD_in; };

	///TODO: maybe follow matlab convention of (start:step:finish)?
	/** generate equally distibuted angles over given range, with input in units of radians */
	void setAnglesRad(const int numAngles_in, const float startAngRad, const float deltaAngRad){
		numAngles = numAngles_in;

		//clean up and set new pointer
		delete angles;
		angles = new float[numAngles];

		// fill angle array
		for(int i=0; i<numAngles; i++){
			angles[i] = startAngRad + float(i)*deltaAngRad;
		}
	}

	void setAnglesDeg(const int  numAngles_in, const float startAngDeg, const float deltaAngDeg){
		setAnglesRad(numAngles_in,startAngDeg * acos(-1.0f)/180.0f, deltaAngDeg * acos(-1.0f)/180.0f);
	}

	/** copys a user's array into the CBCT object */
	void setAnglesRad(const int numAngles_in, const float* userArray){
		numAngles = numAngles_in;

		//clean up and set new pointer
		delete angles;
		angles = new float[numAngles];

		for(int i=0; i<numAngles; i++){
			angles[i] = userArray[i];
		}
	}

	void setDetector(int columns, int rows, float width, float height){
		detector.cols   = columns;   // number of pixel column in detector
		detector.rows   = rows;   // number of pixel rows in detector
		detector.width  = width; // physical width of detector (mm)
		detector.height = height; // physical height of detector
	}

	void setDetectorRes(int columns, int rows){
		detector.cols   = columns;   // number of pixel column in detector
		detector.rows   = rows;   // number of pixel rows in detector
	}

	void setDetectorSize(float width, float height){
		detector.width  = width; // physical width of detector (mm)
		detector.height = height; // physical height of detector
	}

	/** set detector shifts from beams eye view (right,up) */
	void setShifts(float right, float up){
		detector.shiftRt = right;
		detector.shiftUp = up;
	}

	//get functions

	/** access angle array elements, under-runs and over-runs return first and last element respectively */ 
	float getAngle(int i){
		///TODO: make over-runs and under-runs loop around
		if(i < numAngles && i >= 0){
			return angles[i];
		}else if(i >= numAngles){
			return angles[numAngles-1];
		}else{ //under-run
			return angles[0];
		}
	};

	const float* getIsocenterPtr(){return isocenter;}; //I don't like doing this

	/** safe access to isocenter, pointer is hidden */
	float getIsocenterX(){ return isocenter[0]; };
	float getIsocenterY(){ return isocenter[1]; };
	float getIsocenterZ(){ return isocenter[2]; };

	float getSID(){ return SID; };
	float getSAD(){ return SAD; };

	///TODO: change int to long int for future compatability?

	/** return the area (in pixel elements) of the detector (for each image) */
	size_t   getArea()     { return detector.rows*detector.cols; };

	/** return the total number of elements in the projection data */
	size_t   getN()        { return getArea()*numAngles; };

	int   getNumAngles(){ return numAngles; };

	int   getRows()  { return detector.rows;  };
	int   getCols()  { return detector.cols;  };
	float getWidth() { return detector.width; };
	float getHeight(){ return detector.height;};
	float getShiftRt(){ return detector.shiftRt;};
	float getShiftUp(){ return detector.shiftUp;};

  private:
	float* isocenter; // a pointer to a 3 vector defining isocenter in vox coords. (mm)
	float* angles;    // pointer to list of angles (rad)
	int numAngles;    // number of angles in the angles array
	ds_Detector detector;
	float SID;        // source to imager distance (mm)
	float SAD;        // source to axis distance (mm)

};


class drrSuite {

  public:
  ///todo: add member funtion for below
  	bool ROTATION_REUSE_MODE; //for experiment only
  
	bool verbose;        //default true for now

	bool dryRun;         //set to true when you want to see overhead costs of projection
	                     //records all actions (i.e. memCpys) but not kernels
	bool dataOutputOnly;
	bool economy;        //set to true to conserve GPU memory
	float samplingFreq;  //sampling frequency used by TRYLINEAR algorithm
	                     //default set to minimum foxel size when voxels are set
	/** default constructor
	 * default trace algorithm type is Siddon's algorithm
	 * default GPU ID is 0
	 */
	drrSuite(); 

	/** (GPUID,algorithm) constructor
	 * for now, the algorithm type can only be set at creation time to avoid
	 * complications with cleanup and initilization that is algorithm dependent
	 * same goes for the GPU ID, we dont want to manage switching GPUs
	 */
	drrSuite(
		const int gID,
		drrSuite_algorithm_t algorithm
	);

	/** Destructor */
	~drrSuite();

	/** This function sets volume information in class and copies volArray data into the GPU
	 * memory and returns true on success
	 */
	bool setVoxels(
		const int*   volDimension, /**< [in] a 3-vector defining the dimension of the volume array  */
		const float* voxSize,      /**< [in] a 3-vector defining the size of all volume elements    */
		const float* volArray,      /**< [in] an array containing the values for each volume element */
		bool usingDevicePointer = 0
	);

	/** This function creates a single projection from startPt to all points listed in endPtsX,Y,Z
	 * The traceArray pointer is allocated by this function if null and will be of length endPtsLength
	 * If traceArray pointer is already allocated, then projection data is stored starting at this address (be careful)
	 */
	bool getProjection(
		const float* startPt,       /**< [in] a 3-vector defining the starting point of ray trace in volume coord. sys. */
		const float* endPtsX,       /**< [in] an array defining the X element of the ending point of the 
		                                      ray trace in volume coord. sys. (device pointer option)    */
		const float* endPtsY,       /**< [in] an array defining the Y element of the ending point of the
		                                      ray trace in volume coord. sys. (device pointer option)    */
		const float* endPtsZ,       /**< [in] an array defining the Z element of the ending point of the
		                                      ray trace in volume coord. sys. (device pointer option)    */
		const int    endPtsLength,  /**< [in] the length of the X Y and Z end point vectors              */
		const float* volOffset,     /**< [in] a 3-vector defining the origin of the volume coord. sys.
		                                      relative to (0,0,0) corner (isocenter)                     */
		      float* traceArray,    /**< [out] RESULT: array containing the result of the ray traces will
		                                       have length endPtsLength (device pointer option)          */
		      bool   useDevPtrs = 0 /**< [in] optional parameter (default = false) can be set to true to specify endPts
		                                      and traceArray as allocated and set on the device. */
	);

	/** This overloaded version is optimized for multiple CBCT projections specified by the ds_CBCT structure
	 * The traceArray pointer is allocated by this function if null and will be of length endPtsLength
	 * If traceArray pointer is already allocated, then projection data is stored starting at this address (be careful)
	 */
	bool getProjection(
		ds_CBCT* cbctConfig,   /**< [in] the configuration for the CBCT setup */
		float* traceArray,    /**< [out] RESULT */
		bool   useDevPtrs = 0 /**< [in] optional parameter (default = false) can be set to true to specify endPts
		                                and traceArray as allocated and set on the device. */
	);

	bool getArbProjection(
		ds_CBCT* cbctConfig,   /**< [in] the configuration for the CBCT setup */
		float* traceArray,    /**< [out] RESULT */
		bool   useDevPtrs = 0 /**< [in] optional parameter (default = false) can be set to true to specify endPts
		                                and traceArray as allocated and set on the device. */
	);

	void setTimingMode(const char* fileName);

	int getGPU();
	void resetGPU();
  private:
	//made private for now so things can't get jacked up
	bool timingMode;
	char* timingFileName;

	/** helper function */
	__device__ __host__ inline float* max_4(float* v4);
	__device__ __host__ inline float* min_4(float* v4);

	/** core functions */
	__device__ __host__ inline void  siddonInitTraceParameters(const float* ray,float* ti, float* tf, int* startDim, int* isMax);
	__device__ __host__ inline float siddonTrace(const float* ray, float ti, float tf, int startDim, int isMax);
	__device__ __host__ inline float trilinearTrace(float* ray, float ti, float tf, int startDim, int isMax);

	/* cant have kernels in class?
	__global__ void trilinearDRR(
		float* pixPtX,
		float* pixPtY,
		float* pixPtZ,
		float* projection
	);
	__global__ void siddonDRR(
		float* pixPtX,
		float* pixPtY,
		float* pixPtZ,
		float* projection
	);
	*/

	/** wrapper functions */
	

	//private function used to handle two cases since oveloading would have too much code repetition
	bool getProjectionSiddon(
		const float* startPt,       /**< [in] a 3-vector defining the starting point of ray trace in volume coord. sys. */
		const float* endPtsX,       /**< [in] an array defining the X element of the ending point of the 
		                                      ray trace in volume coord. sys. (device pointer option)    */
		const float* endPtsY,       /**< [in] an array defining the Y element of the ending point of the
		                                      ray trace in volume coord. sys. (device pointer option)    */
		const float* endPtsZ,       /**< [in] an array defining the Z element of the ending point of the
		                                      ray trace in volume coord. sys. (device pointer option)    */
		const int    endPtsLength,  /**< [in] the length of the X Y and Z end point vectors              */
		const float* volOffset,     /**< [in] a 3-vector defining the origin of the volume coord. sys.
		                                      relative to (0,0,0) corner (isocenter)                     */
		      float* traceArray,    /**< [out] RESULT: array containing the result of the ray traces will
		                                       have length endPtsLength (device pointer option)          */
		      bool   useDevPtrs,    /**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
		      bool   useCBCTmode,
		      ds_CBCT* CBCT
	);

	bool getProjectionFixedGrid(
//		const float* startPt,       /**< [in] a 3-vector defining the starting point of ray trace in volume coord. sys. */
//		const float* endPtsX,       /**< [in] an array defining the X element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
//		const float* endPtsY,       /**< [in] an array defining the Y element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
//		const float* endPtsZ,       /**< [in] an array defining the Z element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
//		const int    endPtsLength,  /**< [in] the length of the X Y and Z end point vectors */
//		const float* volOffset,     /**< [in] a 3-vector defining the origin of the volume coord. sys. relative to (0,0,0) corner (isocenter) */
		      float* traceArray,    /**< [out] RESULT: array containing the result of the ray traces will have length endPtsLength (device pointer option) */
		      bool   useDevPtrs,    /**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
		      bool   useCBCTmode,
		      ds_CBCT* CBCT
	);

	bool getProjectionFixedGrid_DEV(
//		const float* startPt,       /**< [in] a 3-vector defining the starting point of ray trace in volume coord. sys. */
//		const float* endPtsX,       /**< [in] an array defining the X element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
//		const float* endPtsY,       /**< [in] an array defining the Y element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
//		const float* endPtsZ,       /**< [in] an array defining the Z element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
//		const int    endPtsLength,  /**< [in] the length of the X Y and Z end point vectors */
//		const float* volOffset,     /**< [in] a 3-vector defining the origin of the volume coord. sys. relative to (0,0,0) corner (isocenter) */
		      float* traceArray,    /**< [out] RESULT: array containing the result of the ray traces will have length endPtsLength (device pointer option) */
		      bool   useDevPtrs,    /**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
		      bool   useCBCTmode,
		      ds_CBCT* CBCT
	);


/** depricated, also uses a more GPU ram (no dev pointer support) **/
bool getProjectionFixedGrid_DEV_OLD(
/* will only support CBCT object, not suitable for arbitrary geometry */
		      float* traceArray,    /**< [out] RESULT: array containing the result of the ray traces will have length endPtsLength (device pointer option) */
		      bool   useDevPtrs,    /**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
		      bool   useCBCTmode,
		      ds_CBCT* CBCT
	);

//	bool getProjectionFixedGrid_DEV3D(
//		const float* startPt,       /**< [in] a 3-vector defining the starting point of ray trace in volume coord. sys. */
//		const float* endPtsX,       /**< [in] an array defining the X element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
//		const float* endPtsY,       /**< [in] an array defining the Y element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
//		const float* endPtsZ,       /**< [in] an array defining the Z element of the ending point of the ray trace in volume coord. sys. (device pointer option) */
//		const int    endPtsLength,  /**< [in] the length of the X Y and Z end point vectors */
//		const float* volOffset,     /**< [in] a 3-vector defining the origin of the volume coord. sys. relative to (0,0,0) corner (isocenter) */
//		      float* traceArray,    /**< [out] RESULT: array containing the result of the ray traces will have length endPtsLength (device pointer option) */
//		      bool   useDevPtrs,    /**< [in] optional parameter can be set to true to specify endPts* and traceArray as allocated and set on the device. */
//		      bool   useCBCTmode,
//		      ds_CBCT* CBCT
//	);

	
	//CLASS INFO
	drrSuite_algorithm_t traceType;
	int gpuID;

	//HOST ARRAYS
	//int   volDimension_host[3];
	//float voxSize_host[3];
	bool voxelsSet;

	
	//float* volArray_host;

	//DEVICE POINTERS
	cudaArray* volArray_dev;

	//Device Info
	cudaDeviceProp gpuInfo;

	void construct(); //a place for all common constructor actions
	void setVoxelPlanes(const float*);
	void setCBCTgeometry(ds_CBCT*,int angIdx); //CBCT object, angleNumber
	void setCBCTgeometry(ds_CBCT*,float angRad); //CBCT object, angle (rad)
	void setCBCTgeoFromAxis(ds_CBCT* CBCT, float* projectionAxis); //CBCT object, proj. axis vector (room coord.)
	
	/** overloaded to take the pre-computed source, corner, and detector unit vectors */
	void setArbCBCTgeometry(ds_CBCT*,int angIdx);

	float voxPlanesMin[3]; //plane locations
	float voxPlanesMax[3]; //plane locations
	float source[3];       //source point
	int   voxDim[3];       //size of each dimension
	float voxSize[3];      //spacing of CT planes
	float corner[3];       //spacing of CT planes
	float pixelDown[3];    //spacing of CT planes
	float pixelRight[3];   //spacing of CT planes
	int   maxElement;      //maxElement in projection
	int   blocksPerRow;    //
	bool  CBCTmode;        //specifies CBCT mode for initSiddon
//	float samplingFreq;    //fixed stride sampling freq. (1/mm)
};

//include sorce code
//#include "drrSuite.cu"
//#include "drrSuiteKernels.cu"
#ifdef PYTHON_LIB
#warning ADDING PYTHON FUNCTIONS
#include "drrSuite_externC.cpp" //for python
#endif

#endif
